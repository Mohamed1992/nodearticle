import { Component, OnInit, Input } from '@angular/core';
import { ArticleDetailComponent } from '../article-detail/article-detail.component';
import { ArticleUpdateComponent } from '../article-update/article-update.component';
import { DeleteArticleComponent } from '../delete-article/delete-article.component';

import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { GestionArticleService } from '../service/gestion-article.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.scss']
})
export class ArticleComponent implements OnInit {

  @Input() article
  
  quantitePanier =1
  isAchete = false
  isAdmin = false
  isClient = false
  isDashboard = false
  isEnabled = false
  isPromo = false

  constructor(private serv:GestionArticleService, private modalService: NgbModal, private router : Router) { }

  ngOnInit(): void {
    this.isPromo = this.article.isPromo == "1"
    this.isEnabled = this.article.isEnabled != "1"
    this.isDashboard = this.router.url == "/articles" || this.router.url == "/parametres" 
    this.isAdmin = localStorage.getItem("Role") == "admin"
    
    this.serv.panierChange.subscribe(res=>{
      this.verfiyPanier()
    })

    this.serv.articlesChange.subscribe(res=>{
     
      let articles = res.filter(x => x.id == this.article.id)
      
      if(articles.length > 0 ){
        this.article = articles[0]
        this.isEnabled = this.article.isEnabled != "1"
        this.isPromo = this.article.isPromo == "1"
      }
      
    })


  }

  verfiyPanier(){
    this.quantitePanier = Number(this.serv.verifierAuPanier(this.article))
      
    if(this.quantitePanier > 0){
      this.isAchete = true
    }else{
      this.isAchete = false
    }
  }
  
  openDetailsArticle() {
    this.serv.idArticle = this.article.id
    const modalRef = this.modalService.open(ArticleDetailComponent, {size:"lg"});
    modalRef.componentInstance.name = 'World';
  }

  openModifiedArticle() {
    this.serv.idArticle = this.article.id
    const modalRef = this.modalService.open(ArticleUpdateComponent, {size:"lg"});
    modalRef.componentInstance.name = 'World';
  }

  openSupprimerArticle() {
    this.serv.idArticle = this.article.id
    const modalRef = this.modalService.open(DeleteArticleComponent);
    modalRef.componentInstance.name = 'World';
  }
}
