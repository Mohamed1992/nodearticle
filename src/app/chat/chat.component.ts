import { Component, OnInit } from '@angular/core';
declare var $: any;
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { WebSocketService } from '../service/web-socket.service';
import { GestionUserService } from '../service/gestion-user.service';
import { Router, NavigationEnd } from '@angular/router';
import { ThrowStmt } from '@angular/compiler';
import { interval, timer } from 'rxjs';
@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss']
})
export class ChatComponent implements OnInit {
 
  nbrMessageNonLus 
  nbrCommandesNonLus = 0
  isAdmin = false

  constructor(private fb:FormBuilder, private serv:WebSocketService,  private serv2:GestionUserService,  private router:Router) {
    
    this.serv2.roleChange.subscribe(res =>{
      this.isAdmin = res == "admin"
      if(this.isAdmin){
        this.serv.connectAdmin()
      }else{
        this.serv.connectUser()
      }
    })

    this.serv.numberCommandes.subscribe(res=>{
      this.nbrCommandesNonLus = res
    })

    const timer = interval(60000);
    
    
    
    timer.subscribe((n) => {
      if(this.isAdmin){
        this.serv.connectAdmin()
      }else{
        this.serv.connectUser()
      }

    })

    

    this.serv.listen().subscribe(res=>{
      var messageSocket:any = res
      if(this.user){
        if(this.user.id == messageSocket.idUser && messageSocket.recepteur == "nonAdmin" && !this.isAdmin){
          this. messages.push(messageSocket)
          this.nbrMessageNonLus += 1 
          this.serv.setNumberMessage(this.nbrMessageNonLus) 
          this.renisialiserUser()
       }
      }
    })

   this.serv.numberMessages.subscribe(res=>{
     this.nbrMessageNonLus = res
   })

   this.serv.listenNewVue().subscribe(res => {
    var userVue:any = res
    if(this.user){
      if(this.user.id == userVue.idUser && userVue.recepteur == "nonAdmin"){
        this.renisialiseVue()
     }
    }
    
   })
  }

 

  changeSexe(sexe){
    this.sexe = sexe
    $("#0").removeClass('style-icon-active')
    $("#1").removeClass('style-icon-active')
    
    $("#"+sexe).addClass('style-icon-active')
  }

  formC:FormGroup
  formD:FormGroup
  isConnected = false;
  sexe = 0
  messages = []
  isShowDiscusion = false
  user
  charge = false

  ngOnInit(): void {
    this.user = this.serv.getUserRegister()
    if(this.serv.getUserRegister()){
      this.serv.getNomberMessages(this.serv.getUserRegister().id).subscribe(res =>{
        if(res.status){
          if(res.resultat[0]){
            this.serv.setNumberMessage(res.resultat[0].nomberMessageNonVue)
            this.user = res.resultat[0]
          }
        }
      })
    }

    this.formC=this.fb.group({
      message:['',[Validators.required]],
    })

    this.formD=this.fb.group({
      email:['',[Validators.required]],
      name:['',[Validators.required]],
      sexe:[0 ,[Validators.required]],
    })
  }


  getMessages(){
    this.charge = true
    if(this.user && !this.isAdmin){
      this.serv.getMessagesAdmin(this.user.id).subscribe(res=>{
        if(res.status){
          this.charge = false
           this.messages = res.resultat
           this.addVue()   
        }
      },err =>{
        this.charge = false
        alert("Desole, ilya un probleme de connexion")
      })
    }
  }

  addVue(){
    let nbr = this.user.nomberMessageNonVueAdmin
    for(let i = this.messages.length - 1; i>-1; i--){
       if (nbr > 0 && this.messages[i].recepteur == "admin" ){
        this.messages[i].isVue = false
        nbr -= 1
       }else{
        if(this.messages[i].recepteur == "admin"){
          this.messages[i].isVue = true
        }else{
          this.messages[i].isVue = false
        }
       }
    }
  }

  renisialiseVue(){
    this.user.nomberMessageNonVueAdmin = 0
     for(let i = this.messages.length - 1; i>-1; i--){
        if(this.messages[i].recepteur == "admin"){
          this.messages[i].isVue = true
        }else{
          this.messages[i].isVue = false
        }
       }
  }

  GetThisHidden(){
      this.isShowDiscusion = false
      $(".MyClass").removeClass('style-navbar-mobile-active2')
  }
  
  GetThisDisplayed(){

    if(this.isAdmin){
        this.router.navigate(['/chat'])
        return false
    }

    $(".MyClass").addClass('style-navbar-mobile-active2');
    this.isShowDiscusion = true
    if(this.user){
      this.serv.emitNewVue({idUser:this.user.id, recepteur:"admin"})
      this.scrollMessage()
      this.renisialiserUser()
      this.getMessages()
    }

  }

  renisialiserUser(){
    if(this.isShowDiscusion && this.user){
      this.serv.renisualiserMessagesUser(this.user.id).subscribe(res => {
         this.serv.setNumberMessage(0)
      })
      this.serv.emitNewVue({idUser:this.user.id, recepteur:"admin"})
      this.scrollMessage()    
    }
  }
  
  scrollMessage(){
    setTimeout(() => document.getElementsByClassName('end')[0].scrollIntoView({
      behavior: "smooth",
      block: "center",
      inline: "end"
    }), 100);
  }

  goCommandes(){
    if(this.isAdmin){
      this.router.navigate(['/commandes'])
      return false
    }
  }

  isLoading = false
  envoyerMessage(){
    
    if(!this.user){
      this.isConnected = true
      return true
    }
    
    if(this.formC.value.message == "" || this.formC.value.message ==null ){
      alert("SVP, inserez votre message !")
      return false
    }

    if(this.isLoading){
      return false
    }
    this.isLoading = true

    this.serv.newMessage(this.formC.value).subscribe(res => {
      if(res.status){
          this.formC.reset()
          let message = res.resultat
          message.isVue = false
          this.messages.push(res.resultat)  
          this.serv.emit(res.resultat)    
          this.scrollMessage()
          this.isLoading = false 
          this.user.nomberMessageNonVueAdmin +=1
      }
    },err =>{
      alert("Désole, ilya un probléme connexion.")  
    })
  
  }
  
  register(){
    if(this.formD.value.email == ""){
      alert("SVP, inserez votre email !")
      return false
    }else if(this.formD.value.name == ""){
      alert("SVP, inserez votre nom !")
      return false
    }

    this.formD.value.sexe = this.sexe
  
    this.serv.register(this.formD.value).subscribe(res => {
      if(res.status){
        this.user = res.resultat
        this.serv.setNewRegister(res.resultat)
        this.serv.emitNewRegister("new")
        this.isConnected = false
      }
    },err =>{
      alert("Désole, ilya un probléme connexion")  
    })
  }




}
