import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PagesArticlesComponent } from './pages-articles.component';

describe('PagesArticlesComponent', () => {
  let component: PagesArticlesComponent;
  let fixture: ComponentFixture<PagesArticlesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PagesArticlesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PagesArticlesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
