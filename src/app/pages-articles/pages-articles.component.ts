import { Component, OnInit } from '@angular/core';
import { GestionCategorieService } from '../service/gestion-categorie.service';

@Component({
  selector: 'app-pages-articles',
  templateUrl: './pages-articles.component.html',
  styleUrls: ['./pages-articles.component.scss']
})
export class PagesArticlesComponent implements OnInit {

  categories

  constructor(private serv:GestionCategorieService) {
    this.serv.categorieFiltrationChange.subscribe(res =>{
      this.categories = res
    })
  }
  
  
  ngOnInit(): void {
    
  }

}
