import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { GestionUserService } from '../service/gestion-user.service';
import { GestionCategorieService } from '../service/gestion-categorie.service';
declare var $: any;

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  constructor( private serv:GestionUserService, private router:Router ,private fb:FormBuilder, private serv2:GestionCategorieService) { 
  }

  formC:FormGroup

  tabCategoriesGlobal = []

  tabCategories = []

  ngOnInit(): void {
    this.formC=this.fb.group({
      nom:['',[Validators.required]],
      email:['',[Validators.required]],
      subject:['',[Validators.required]],
      company:['',[Validators.required]],
      message:['',[Validators.required]],
    })

    if(this.serv2.tabCategories.length > 0){
      this.tabCategoriesGlobal = this.serv2.tabCategories 
    }else{
      this.serv2.listCategories().subscribe(res=>{
        this.serv2.setCategories(res.resultat)
        this.tabCategoriesGlobal = res.resultat
        this.serv2.tabCategories = this.tabCategoriesGlobal
      })
    }

  }

  envoyerMessage(){
    if(this.controleInput(this.formC.value)){
      this.formC.value.nom = "abonne" 
      this.formC.value.subject = "abonne"
      this.formC.value.company = "abonne" 
      this.formC.value.message = " Nouveau abonné "+  this.formC.value.email
      
      this.serv.envoyerEmail(this.formC.value).subscribe(res => {
         alert("Votre abonnement est réussie")  
       }, err =>{
         alert("erreur")
       })
    }
  }

  controleInput(obj){
    if(obj.email == ""){
      alert("SVP, inserer l'email") 
      return false
    }
     
    return true
  }

  goPageArticles(event){
    var categorie = this.tabCategoriesGlobal.filter(x=> x.nom == event.target.id)[0]
    this.serv2.setCategorieFiltration({categorie:categorie.id, sousCategorie: "0", sousSousCategorie: "0"}) 
    this.router.navigate(['/'+event.target.id])
    this.clickTop()
  }

  goPresentation(){
    this.router.navigate(['/presentation'])
    $("html, body").animate({scrollTop : 500},700);
  }

  goContact(){
    this.router.navigate(['/contact'])
    this.clickTop()
  }


  clickTop(){
    $("html, body").animate({scrollTop : 500},700);
  }

}
