import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-index-bar',
  templateUrl: './index-bar.component.html',
  styleUrls: ['./index-bar.component.scss']
})
export class IndexBarComponent implements OnInit {

  constructor() { }

  @Input() nom:String=""
  ngOnInit(): void {
  }

}
