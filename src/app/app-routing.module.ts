import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddArticleComponent } from './add-article/add-article.component';
import { ListArticleComponent } from './list-article/list-article.component';
import { GuardGuard } from './service/guard.guard';
import { AccueilComponent } from './accueil/accueil.component';
import { ArticleDetailComponent } from './article-detail/article-detail.component';
import { RegisterComponent } from './register/register.component';
import { ContactComponent } from './contact/contact.component';
import { PresentationComponent } from './presentation/presentation.component';
import { BoutiquesComponent } from './boutiques/boutiques.component';
import { ClientsComponent } from './clients/clients.component';
import { CommandesComponent } from './commandes/commandes.component';
import { ProfilComponent } from './profil/profil.component';
import { CategoriesComponent } from './categories/categories.component';
import { PagePanierComponent } from './page-panier/page-panier.component';
import { PageRechercheComponent } from './page-recherche/page-recherche.component';
import { ParametresComponent } from './parametres/parametres.component';
import { PagesArticlesComponent } from './pages-articles/pages-articles.component';
import { MarquesComponent } from './marques/marques.component';
import { LibrairieComponent } from './librairie/librairie.component';
import { InformatiqueComponent } from './informatique/informatique.component';
import { CosmetiqueComponent } from './cosmetique/cosmetique.component';
import { ElectromenageComponent } from './electromenage/electromenage.component';
import { TissuComponent } from './tissu/tissu.component';
import { MaisonComponent } from './maison/maison.component';
import { TelephoneComponent } from './telephone/telephone.component';
import { CadeauxComponent } from './cadeaux/cadeaux.component';
import { BoutiqueComponent } from './boutique/boutique.component';
import { JouetComponent } from './jouet/jouet.component';
import { ValideEmailComponent } from './valide-email/valide-email.component';
import { MessagesPageComponent } from './messages-page/messages-page.component';

const routes: Routes = [
  
  {path: 'newArticle',component:AddArticleComponent,canActivate:[GuardGuard]},
  {path: 'articles',component:ListArticleComponent,canActivate:[GuardGuard]},
  {path: '',redirectTo:'accueil',pathMatch:"full"},
  {path: 'ad' , component: ArticleDetailComponent },
  
  {path: 'accueil' , component: AccueilComponent },
  {path: 'presentation' , component: PresentationComponent },
  {path: 'contact',component: ContactComponent},
  
  {path: 'pageArticles' , component: PagesArticlesComponent },
  {path: 'panier' , component: PagePanierComponent },
  {path: 'recherche' , component: PageRechercheComponent },

  {path: 'marques' , component: MarquesComponent },
  {path: 'boutiques' , component: BoutiquesComponent ,canActivate:[GuardGuard] },
  {path: 'clients' , component: ClientsComponent ,canActivate:[GuardGuard]},
  {path: 'commandes' , component: CommandesComponent ,canActivate:[GuardGuard] },
  {path: 'profil' , component: ProfilComponent ,canActivate:[GuardGuard]},
  {path: 'categories' , component: CategoriesComponent ,canActivate:[GuardGuard] },
  {path: 'parametres',component: ParametresComponent},
 
 
  {path: 'Librairie',component: LibrairieComponent},
  {path: 'Informatique',component: InformatiqueComponent},
  {path: 'Téléphonie',component: TelephoneComponent},
  {path: 'Cadeaux',component: CadeauxComponent},
  {path: 'Tissu',component: TissuComponent},
  {path: 'Mode',component: BoutiqueComponent},
  {path: 'Maison et Loisir',component: MaisonComponent},
  {path: 'Électroménager',component: ElectromenageComponent},
  {path: 'Cosmétique',component: CosmetiqueComponent},
  {path: 'Jouet',component: JouetComponent},
  {path: 'chat',component: MessagesPageComponent},
  
  {path: 'ValidEmail/:id',component: ValideEmailComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
