import { Component, OnInit} from '@angular/core';
import { GestionArticleService } from '../service/gestion-article.service';

@Component({
  selector: 'app-page-recherche',
  templateUrl: './page-recherche.component.html',
  styleUrls: ['./page-recherche.component.scss']
})
export class PageRechercheComponent implements OnInit {

  page=1
  totalPage=1
  search=""
  tabArticles = []
  charge = false
  
  constructor(private serv:GestionArticleService) {
    this.serv.searchChange.subscribe(res=>{
      this.search = res
      this.page = 1
      this.charge = true
      this.serv.articlesBySearch(this.search,this.page).subscribe(res=>{
        this.charge = false
        this.tabArticles=res.resultat.docs
        this.page = res.resultat.page
        this.totalPage = res.resultat.pages
      })
    })

    
   }

  ngOnInit(): void {
      
  }

  setPage(newPage: number) {
    this.page = newPage
    this.charge = true
    this.serv.articlesBySearch(this.search,this.page).subscribe(res=>{
      this.charge = false
      this.tabArticles=res.resultat.docs
      this.page = res.resultat.page
      this.totalPage = res.resultat.pages
    })
  }

}

