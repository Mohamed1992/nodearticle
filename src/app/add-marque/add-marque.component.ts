import { Component, OnInit } from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { GestionCategorieService } from '../service/gestion-categorie.service';
import { GestionMarquesService } from '../service/gestion-marques.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
declare var $: any;

@Component({
  selector: 'app-add-marque',
  templateUrl: './add-marque.component.html',
  styleUrls: ['./add-marque.component.scss']
})
export class AddMarqueComponent implements OnInit {

  formC:FormGroup
  
  isLoading = false

  idCategorie = '1'

  idSousCategorie = '0'

  tabSousCategories = []

  tabCategoriesGlobal = []

  tabCategories = []

 
  constructor(private fb:FormBuilder, public activeModal: NgbActiveModal, private serv:GestionCategorieService, private serv2:GestionMarquesService) {
    
    this.serv.listCategories().subscribe(res=>{
      
      this.serv.setCategories(res.resultat)

      this.tabCategoriesGlobal = res.resultat
      
      this.serv.tabCategories = this.tabCategoriesGlobal
     
      this.tabCategories = this.tabCategoriesGlobal.filter(x=> x.categorie == "0")  

      for(var i = 0; i<this.tabCategories.length; i++ ){
        var listSousCategories = this.tabCategoriesGlobal.filter(x=> x.categorie == this.tabCategories[i].id)
        
        var sousCategoriesWithSousSous = []
        
        for(var j = 0; j<listSousCategories.length; j++ ){
          sousCategoriesWithSousSous.push({sousCategorie: listSousCategories[j], sousSousCategories: this.tabCategoriesGlobal.filter(x=> x.categorie == listSousCategories[j].id) })
        }

        this.tabSousCategories.push({categorie:this.tabCategories[i], sousCategories:  sousCategoriesWithSousSous })
      }

    })

    this.formC=this.fb.group({
      nom:['',[Validators.required]],
      categorie:['0',[Validators.required]],
      sousCategorie:['0',[Validators.required]],
      sousSousCategorie:['0',[Validators.required]],
    })
  }

  ngOnInit(): void {
  }

  addMarque(){
    
    if(this.formC.value.categorie == "" && this.formC.value.categorie == undefined){
      this.formC.value.categorie = this.tabCategories[0].id
    }

    if(this.formC.value.sousCategorie == "" || this.formC.value.sousCategorie == "0"){
      this.formC.value.sousCategorie = "0"
    }else{
      let listCats = this.tabCategoriesGlobal.filter(x=> x.id == this.formC.value.sousCategorie)
     console.log(listCats)
      if(listCats[0].categorie != this.formC.value.categorie ){
        this.formC.value.sousCategorie = "0"
      }

    }

    if(this.formC.value.sousSousCategorie == ""){
      this.formC.value.sousSousCategorie = "0"
    }else{
      let listCats = this.tabCategoriesGlobal.filter(x=> x.id == this.formC.value.sousSousCategorie)
      if(listCats.length > 0 ){
      
        if(listCats[0].categorie != this.formC.value.sousCategorie ){
          this.formC.value.sousSousCategorie = "0"
        }
      }else{
        this.formC.value.sousSousCategorie = "0"
      }

    }
   
    if(this.formC.value.nom == ""){
      alert("SVP inserer le nom")
    }else if(!this.isLoading){

      this.isLoading = true
      this.serv2.newMarque(this.formC.value).subscribe(res => {
        if(res.status){
          this.isLoading = false
          alert("marque ajouter")
          this.activeModal.close()
          this.serv2.setLoading()
        }else{
          this.isLoading = false
          alert("erreur")
        }
      }, err =>{
        this.isLoading = false
        alert(err._body)
        
      })
    }
   
    console.log(this.formC.value)
  }

  changeCategorie(event){
    this.formC.value.sousCategorie == ''
    if(event.target.value !=''){
      this.idCategorie =event.target.value
    }
     
    var elements = document.getElementsByClassName("sous-categorie-22")
   for (let i = 0; i <elements.length; i++){
      
      if(elements[i].id == this.idCategorie){
         elements[i].setAttribute("style", "  ")
    
      }else{
        elements[i].setAttribute("style", "visibility: hidden;")
      }
    }

    this.initializeSousSousCategorie()
    this.initializeSousCategorie()
    
    var sousElements = document.getElementsByClassName("sous-categorie-33")
   
  

    for (let i = 0; i <sousElements.length; i++){
      
      if(sousElements[i].id == "1000"){
        sousElements[i].setAttribute("style", "  ")
      }else{
        sousElements[i].setAttribute("style", "visibility: hidden;")
      }
    }

  }

  changeSousCategorie(event){
   console.log(event.target.value)
    if(event.target.value !=''){
      this.idSousCategorie =event.target.value
    }
     
    var elements = document.getElementsByClassName("sous-categorie-33")
   
  

    for (let i = 0; i <elements.length; i++){
      
      if(elements[i].id == this.idSousCategorie){
         elements[i].setAttribute("style", "  ")
      }else{
        elements[i].setAttribute("style", "visibility: hidden;")
      }
    }

    this.initializeSousSousCategorie()

  }

  initializeSousCategorie(){
    var elements = document.getElementsByClassName("sous-categorie-22")
   
    for (let i = 0; i <elements.length; i++){
      $('#'+elements[i].id).val(0) 
    }

  }

  initializeSousSousCategorie(){
    var elements = document.getElementsByClassName("sous-categorie-33")
   
    for (let i = 0; i <elements.length; i++){
      $('#'+elements[i].id).val(0) 
    }
  }


}
