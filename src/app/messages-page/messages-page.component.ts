import { Component, OnInit } from '@angular/core';
declare var $: any;
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { WebSocketService } from '../service/web-socket.service';

@Component({
  selector: 'app-messages-page',
  templateUrl: './messages-page.component.html',
  styleUrls: ['./messages-page.component.scss']
})
export class MessagesPageComponent implements OnInit {

  loadingMessages = true

  constructor(private fb:FormBuilder, private serv:WebSocketService) {
    this.serv.listen().subscribe(res=>{
      var messageSocket:any = res
      if(this.user){
        if(this.user.id == messageSocket.idUser && messageSocket.recepteur == "admin"){
          this.messages.push(messageSocket)
          this.serv.renisualiserMessagesAdmin(this.user.id).subscribe(res => {
          })
          this.scrollMessage()
          this.renisialiseVue()
          if(this.serv.varNumberMessages > 0){
            this.serv.setNumberMessage(this.serv.varNumberMessages - 1)
          }
        }else{
          for(let i =0;i <this.users.length; i++){
            if(this.users[i].id == messageSocket.idUser && messageSocket.recepteur == "admin"){
              this.users[i].nomberMessageNonVueAdmin +=1
            }
          }
        }
      }
    })

    this.serv.listenNewVue().subscribe(res => {
      var userVue:any = res
      if(this.user){
        if(this.user.id == userVue.idUser && userVue.recepteur == "admin"){
           this.renisialiseVue()  
        }
          
        for(let i =0;i<this.users.length;i++){
            if(this.users[i].id == userVue.idUser){
              this.users[i].nomberMessageNonVue = 0
            }
        }  
      
      }
    })

  }

  scrollMessage(){
    setTimeout(() => document.getElementsByClassName('end')[0].scrollIntoView({
      behavior: "smooth",
      block: "center",
      inline: "end"
    }), 100);
  }
  
  users=[]
  user
  formD:FormGroup

  ngOnInit(): void {
    this.getUsers()

    this.serv.listenNewRegister().subscribe(res=>{
      if(localStorage.getItem("Role") == "admin"){
        this.getUsers()
      }
    })

    this.serv.users.subscribe(res =>{
          if(res.length > 0){
            this.loadingMessages = false    
            this.users = res
            this.renisialiserAdmin(0)
          }    
    })

    this.formD=this.fb.group({
      message:['',[Validators.required]],
    })

  }

  getUsers(){
    this.serv.getUsersAdmin().subscribe(res =>{
      if(res.status){
        this.serv.setUsers(res.resultat)
        this.serv.setNumberMessage(this.serv.getNumberMessagesOfAdmin())
      }
    },err =>{
      alert("Désole, ilya problem connexion")
    })

  }

  renisialiserAdmin(i){
      this.user = this.users[i]
      
      this.getMessages()

      this.serv.emitNewVue({idUser:this.user.id, recepteur:"nonAdmin"})

      const nbr = this.serv.varNumberMessages - this.user.nomberMessageNonVueAdmin
      if(nbr > 0){
        this.serv.setNumberMessage(nbr)
      }else{
        this.serv.setNumberMessage(0)
      }
      
      this.users[i].nomberMessageNonVueAdmin = 0
      
      this.serv.renisualiserMessagesAdmin(this.user.id).subscribe(res => {
        this.scrollMessage()
      })

 }
  
  
  selectUser(event){
      event.target.id
      for(let i =0;i<this.users.length;i++){
          if(this.users[i].id == event.target.id){
            this.renisialiserAdmin(i)
          }
      }
  }

  messages=[]
  getMessages(){
    this.loadingMessages = true
    this.serv.getMessagesAdmin(this.user.id).subscribe(res=>{
      if(res.status){
         this.messages = res.resultat 
         this.addVue()
         this.loadingMessages = false
      }
    },err =>{
      alert("Desole, ilya un probleme de connexion")
    })
  }

  addVue(){
    let nbr = this.user.nomberMessageNonVue
    
    for(let i = this.messages.length - 1; i>-1; i--){
       if (nbr > 0 && this.messages[i].recepteur == "nonAdmin" ){
        this.messages[i].isVue = false
        nbr -= 1
       }else{
        if(this.messages[i].recepteur == "nonAdmin"){
          this.messages[i].isVue = true
        }else{
          this.messages[i].isVue = false
        }
       }
    }
  }

  renisialiseVue(){
    this.serv.emitNewVue({idUser:this.user.id, recepteur:"nonAdmin"})
    for(let i = this.messages.length - 1; i>-1; i--){
        if(this.messages[i].recepteur == "nonAdmin"){
          this.messages[i].isVue = true
        }else{
          this.messages[i].isVue = false
        }
        
     }
  }

  isLoading = false
  envoyerMessage(){
    
    if(this.formD.value.message == "" || this.formD.value.message ==null ){
      alert("SVP, inserez votre message !")
      return false
    }

    if(this.isLoading){
      return false
    }

    this.isLoading = true
    
    this.serv.newMessageAdmin(this.formD.value, this.user).subscribe(res => {
      if(res.status){
          this.formD.reset()
          let message = res.resultat
          message.isVue = false
          this.messages.push(message)
          this.serv.emit(res.resultat)  
          this.user.nomberMessageNonVue +=1
          this.scrollMessage()
          this.isLoading = false   
      }
    },err =>{
      alert("Désole, ilya un probléme connexion.")  
    })
  
  }

}
