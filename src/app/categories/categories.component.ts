import { Component, OnInit } from '@angular/core';
import { GestionCategorieService } from '../service/gestion-categorie.service';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AddCategorieComponent } from '../add-categorie/add-categorie.component';
import { DeleteCategorieComponent } from '../delete-categorie/delete-categorie.component';
import { UpdateCategorieComponent } from '../update-categorie/update-categorie.component';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.scss']
})
export class CategoriesComponent implements OnInit {

  tabCategoriesGlobal = []

  tabCategories = []

  tabSousCategories = []
  
  tabTableau = []
  
  constructor(private serv:GestionCategorieService, private modalService: NgbModal) { }

  ngOnInit(): void {
   this.serv.loadingChange.subscribe(res =>{
    this.getCategorie()   
   }) 
  }

  getCategorie(){
    this.tabCategoriesGlobal = []
    this.tabCategories = []
    this.tabSousCategories = []
    this.tabTableau = []
    
    this.serv.listCategories().subscribe(res=>{
      
      this.serv.setCategories(res.resultat)
      this.tabCategoriesGlobal = res.resultat
      this.serv.tabCategories = this.tabCategoriesGlobal
     
      this.tabCategories = this.tabCategoriesGlobal.filter(x=> x.categorie == "0")  
      
      var styleName = ""
      var className = "categorie-niveau-1 categorie-Active"
      
      this.tabSousCategories.push({categorie:{nom:"niveau-0", id:"0"}, sousCategories: [] ,style:styleName, class:className})
      
      styleName = "display: none;"
      className = "categorie-niveau-1"
      
      for(var i = 0; i<this.tabCategories.length; i++ ){
         this.tabSousCategories.push({categorie:this.tabCategories[i], sousCategories: this.tabCategoriesGlobal.filter(x=> x.categorie == this.tabCategories[i].id) ,style:styleName, class:className})
      }
    
        this.tabTableau = this.tabCategories
      
     
    })
   
  }

  choisirCategorie(event){

    this.tabTableau = this.tabCategoriesGlobal.filter(x=> x.categorie === event.target.id)
   
    for (let i = 0; i <this.tabSousCategories.length; i++){
      if(this.tabSousCategories[i].categorie.id == event.target.id){
        this.tabSousCategories[i].class = "categorie-niveau-1 categorie-Active"
        this.tabSousCategories[i].style = ""
      }else{
        this.tabSousCategories[i].class = "categorie-niveau-1"
        this.tabSousCategories[i].style = "display:none;"
      }
    }

    var elements2 = document.getElementsByClassName("categorie-niveau-2 categorie-Active")
    
    if(elements2.length > 0){
      elements2[0].setAttribute("class", "categorie-niveau-2")
    }
  }

  choisirSousCategorie(event){
    this.tabTableau = this.tabCategoriesGlobal.filter(x=> x.categorie === event.target.id)

    var elements = document.getElementsByClassName("categorie-niveau-2")
    
    for (let i = 0; i <elements.length; i++){
      if(elements[i].id == event.target.id){
        elements[i].setAttribute("class", "categorie-niveau-2 categorie-Active")
      }else{
        elements[i].setAttribute("class", "categorie-niveau-2")
     
      }
    }    
  }

  AjouterCategorie(){
    const modalRef = this.modalService.open(AddCategorieComponent);
    modalRef.componentInstance.name = 'World';
  }

  openModifiedCategorie(event){
    this.serv.idCategorie = event.target.id
    const modalRef = this.modalService.open(UpdateCategorieComponent);
    modalRef.componentInstance.name = 'World';
  }

  openDeleteCategorie(event){
    this.serv.idCategorie = event.target.id
    const modalRef = this.modalService.open(DeleteCategorieComponent);
    modalRef.componentInstance.name = 'World';
  }

}
