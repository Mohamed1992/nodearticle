import { Component, OnInit, Input, SimpleChanges  } from '@angular/core';
import { GestionArticleService } from '../service/gestion-article.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-articles',
  templateUrl: './articles.component.html',
  styleUrls: ['./articles.component.scss']
})

export class ArticlesComponent implements OnInit {

  tabArticles = []
  isDashboard = false
  loading = false
  page=1
  totalPage

  search2=""

  @Input() objectFilter:any
 
  constructor(private serv:GestionArticleService, private router : Router) {
   
     this.search2 = this.serv.varSearch2
  }

  
  ngOnInit(): void {
    this.serv.searchChange2.subscribe(res=>{
      if(this.search2 != res){
          this.search2 = res
          this.getArticle2()
      }
    })

    this.serv.loadingChange.subscribe(res=>{
      if(!this.loading){
          this.getArticle2()
      }
    })

   
  }

  getArticle2(){
    this.isDashboard = this.router.url == "/articles"
    this.loading = true
    let request = {marque:this.objectFilter.marque, categorie: this.objectFilter.categorie, sousCategorie:this.objectFilter.sousCategorie, sousSousCategorie: this.objectFilter.sousSousCategorie, page: this.page, minPrice: this.objectFilter.minPrice, maxPrice: this.objectFilter.maxPrice}
    if(!this.isDashboard){
      this.serv.listArticlesByCategories(request).subscribe(res=>{
        this.serv.setArticles(res.resultat.docs)
        this.tabArticles = res.resultat.docs
        this.totalPage = res.resultat.pages
        this.page = res.resultat.page
        this.loading = false
      })
    }else{
      this.serv.listArticlesByCategoriesDashboard(request).subscribe(res=>{
        this.serv.setArticles(res.resultat.docs)
        this.tabArticles = res.resultat.docs
        this.totalPage = res.resultat.pages
        this.page = res.resultat.page
        this.loading = false
     })
    }
  }

  ngOnChanges(changes: SimpleChanges) {
      if(this.search2 != ""){
        this.serv.setSearch2("")
        this.page = 1
      }else{
        this.page = 1
        this.getArticle2()
      }
    
  }

  setPage(newPage: number) {
    this.page = newPage
    this.getArticle2()
  }

  

}
