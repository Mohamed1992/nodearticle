
import { Component, OnInit, Input, SimpleChanges } from '@angular/core';
import { GestionCategorieService } from '../service/gestion-categorie.service';
import { GestionArticleService } from '../service/gestion-article.service';
declare var $: any;

@Component({
  selector: 'app-page-articles',
  templateUrl: './page-articles.component.html',
  styleUrls: ['./page-articles.component.scss']
})
export class PageArticlesComponent implements OnInit {

  constructor(private serv :GestionCategorieService, private serv2 :GestionArticleService) { }
  
  tabSousCategories=[]
  tabCategories=[]
  tabListCategories=[]
  
  tabMarquesGlobal=[]
  tabMarques = []
  listMarques = []

  
  @Input() categories 

  categorie
  sousCategorie
  sousSousCategorie
  
  marque = "0"
  nomMarque = "0"  

  minPrice = 0
  maxPrice = 0
  
  objectFilter
  ngOnInit(): void {
  
  }

  ngOnChanges(changes: SimpleChanges) {
      this.categorie = this.categories.categorie
      this.sousCategorie = this.categories.sousCategorie
      this.sousSousCategorie = this.categories.sousSousCategorie
      this.nomMarque = "0"
    
      this.minPrice = 0
      this.maxPrice = 0
      
      this.objectFilter = {categorie: this.categorie, sousCategorie: this.sousCategorie, sousSousCategorie: this.sousSousCategorie, marque:this.nomMarque, maxPrice: this.maxPrice, minPrice: this.minPrice}

      this.getCategorie()   
  }


  triCategories(){

     
      this.listMarques = this.tabMarquesGlobal.filter(x=> x.categorie == this.categorie)
      this.setMarques(this.categorie, "0", "0")
      this.inisialiseMarque()

      this.tabCategories.push({nom:"tout", id: "0", categorie: this.categorie})
     
       
      if( this.tabCategories[0].id == this.sousCategorie){
        this.tabListCategories.push({categorie:this.tabCategories[0], class: "categorie-niveau-1 categorie-Active", style: "",list: [] }) 
      }else{
        this.tabListCategories.push({categorie:this.tabCategories[0], class: "categorie-niveau-1", style: "display:none",list: [] }) 
      }
    
      this.tabCategories= this.tabSousCategories.filter(x=> x.categorie === this.categorie)
     
      for (let i = 0; i <this.tabCategories.length; i++){

        var sousSousCategories = this.tabSousCategories.filter(x=> x.categorie == this.tabCategories[i].id)
        var sousSousCategories2 = []
        for (let j = 0; j < sousSousCategories.length; j++){
          
           if( sousSousCategories[j].id == this.sousSousCategorie){
             sousSousCategories2.push({sousSousCategorie:sousSousCategories[j], class:"categorie-niveau-2 categorie-Active"})  
           }else{
             sousSousCategories2.push({sousSousCategorie:sousSousCategories[j], class:"categorie-niveau-2"})
           }
          
        }

        if( this.tabCategories[i].id == this.sousCategorie){
          this.tabListCategories.push({categorie:this.tabCategories[i], class: "categorie-niveau-1 categorie-Active", style: "",list: sousSousCategories2 }) 
        }else{
          this.tabListCategories.push({categorie:this.tabCategories[i], class: "categorie-niveau-1", style: "display:none",list: sousSousCategories2 }) 
        }
      }

      
  }

  getCategorie(){
    this.marque = '0'
    this.nomMarque = '0'
    this.inisialiseMarque()
    
    this.tabListCategories = []
    this.tabCategories = []
    
    if(this.tabSousCategories.length == 0){
    
      this.serv.listCategories().subscribe(res=>{
        this.serv.setCategories(res.resultat)
        this.tabSousCategories = res.resultat
  
        this.serv.tabCategories = this.tabSousCategories
  
        this.tabMarquesGlobal = res.marques 
        
        this.triCategories()
      })
    }else{
      this.triCategories()
    }    

  }

  choisirCategorie(event){
    
    
    this.sousCategorie = event.target.id
    
    if(this.sousCategorie != "0"){
      this.setMarques("0", this.sousCategorie, "0")
      this.inisialiseMarque()
    }else{
      this.setMarques( this.categorie,"0", "0")
      this.inisialiseMarque()
    }
    
    this.sousSousCategorie = "0"
    
    var elements = document.getElementsByClassName("categorie-niveau-1")
    var blocks = document.getElementsByClassName("block-sous-categorie")
    
    for (let i = 0; i <elements.length; i++){
      if(elements[i].id == this.sousCategorie){
        elements[i].setAttribute("class", "categorie-niveau-1 categorie-Active")
      }else{
        elements[i].setAttribute("class", "categorie-niveau-1")
     
      }
    }

    for (let i = 0; i <blocks.length; i++){
      if(blocks[i].id == this.sousCategorie){
        blocks[i].setAttribute("style", "")
      }else{
        blocks[i].setAttribute("style", "display:none")
      }
    }

    var elements2 = document.getElementsByClassName("categorie-niveau-2 categorie-Active")
    
    if(elements2.length > 0){
      elements2[0].setAttribute("class", "categorie-niveau-2")
    }

    this.objectFilter = {categorie: this.categorie, sousCategorie: this.sousCategorie, sousSousCategorie: this.sousSousCategorie, marque:this.nomMarque, maxPrice: this.maxPrice, minPrice: this.minPrice}

  }

  choisirSousCategorie(event){
    this.sousSousCategorie = event.target.id
    
    
    this.setMarques("0", "0", this.sousSousCategorie)
    this.inisialiseMarque()
    
    var elements = document.getElementsByClassName("categorie-niveau-2")
    
    for (let i = 0; i <elements.length; i++){
      if(elements[i].id == event.target.id){
        elements[i].setAttribute("class", "categorie-niveau-2 categorie-Active")
      }else{
        elements[i].setAttribute("class", "categorie-niveau-2")
     
      }
    } 

    this.objectFilter = {categorie: this.categorie, sousCategorie: this.sousCategorie, sousSousCategorie: this.sousSousCategorie, marque:this.nomMarque, maxPrice: this.maxPrice, minPrice: this.minPrice}
   
  }

  choisirMarques(event){
    this.marque = event.target.id
  
    var listMarques2 = this.listMarques
    
    if(this.marque != "0"){
      this.nomMarque = listMarques2.filter(x => (x.id == this.marque))[0].nom
    }else{
      this.nomMarque = "0"
    }

    var elements = document.getElementsByClassName("marque-niveau-1")
    
    for (let i = 0; i <elements.length; i++){
      if(elements[i].id == event.target.id){
        elements[i].setAttribute("class", "marque-niveau-1 categorie-Active")
      }else{
        elements[i].setAttribute("class", "marque-niveau-1")
     
      }
    }
    
    this.objectFilter = {categorie: this.categorie, sousCategorie: this.sousCategorie, sousSousCategorie: this.sousSousCategorie, marque:this.nomMarque, maxPrice: this.maxPrice, minPrice: this.minPrice}

  }

  inisialiseMarque(){
    var elements = document.getElementsByClassName("marque-niveau-1")
    this.marque = "0"
    this.nomMarque = "0"
    this.maxPrice = 0
    this.minPrice = 0

    for (let i = 0; i <elements.length; i++){
      if(elements[i].id == '0'){
        elements[i].setAttribute("class", "marque-niveau-1 categorie-Active")
      }else{
        elements[i].setAttribute("class", "marque-niveau-1")
      }
    }    
  }


  setMarques(categorieId, sousCategorieId, sousSousCategorieId){
    
    var listMarques2 = this.listMarques
    
    if(categorieId != "0"){
      listMarques2 = listMarques2.filter(x => (x.categorie == categorieId))
      this.filterMarques(listMarques2)  
    }else if(sousCategorieId != "0"){
      listMarques2 = listMarques2.filter(x => (x.sousCategorie == sousCategorieId))
      this.filterMarques(listMarques2)
    }else if(sousSousCategorieId != "0"){
      listMarques2 = listMarques2.filter(x => x.sousSousCategorie == sousSousCategorieId)
      this.filterMarques(listMarques2)
    }

  }

  filterMarques(listMarques2){
    var newMarques = []
      
    for(var i =0; i<listMarques2.length;i++){
      var somme = 0
       for(var j = 0; j < newMarques.length;j++){
         if(listMarques2[i].nom == newMarques[j].nom){
            somme++
         }
       }
       if(somme == 0){
          newMarques.push(listMarques2[i])
       }
    }
  
    this.tabMarques = newMarques
  }
  
  setPrixEvent(prices) {
    this.minPrice = prices.minPrice
    this.maxPrice = prices.maxPrice

    this.objectFilter = {categorie: this.categorie, sousCategorie: this.sousCategorie, sousSousCategorie: this.sousSousCategorie, marque:this.nomMarque, maxPrice: this.maxPrice, minPrice: this.minPrice}

  }
  
}
