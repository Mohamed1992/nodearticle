import { Component, OnInit } from '@angular/core';
import { GestionMarquesService } from '../service/gestion-marques.service';
import { GestionCategorieService } from '../service/gestion-categorie.service';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AddMarqueComponent } from '../add-marque/add-marque.component';
import { ModefierMarqueComponent } from '../modefier-marque/modefier-marque.component';
import { DeleteMarqueComponent } from '../delete-marque/delete-marque.component';


@Component({
  selector: 'app-marques',
  templateUrl: './marques.component.html',
  styleUrls: ['./marques.component.scss']
})
export class MarquesComponent implements OnInit {

  tabMarques=[]
  page = 1
  totalPage=1
  isDisponible = true
  
  tabCategoriesGlobal = []

  tabCategories = []

  tabSousCategories = []
  
  tabTableau = []
 
  constructor(private serv:GestionMarquesService, private modalService: NgbModal, private serv2:GestionCategorieService) { 
    this.getCategorie()
  }

  getCategorie(){
    this.tabCategoriesGlobal = []
    this.tabCategories = []
    this.tabSousCategories = []
    this.tabTableau = []
    
    this.serv2.listCategories().subscribe(res=>{
      
      this.serv2.setCategories(res.resultat)
      this.tabCategoriesGlobal = res.resultat
      this.serv2.tabCategories = this.tabCategoriesGlobal
     
      this.tabCategories = this.tabCategoriesGlobal.filter(x=> x.categorie == "0")  
     
      this.serv.loadingChange.subscribe(res =>{
        if(this.isDisponible){
          this.getMarques()

         
          for(var i = 0; i<this.tabSousCategories.length; i++ ){
            if(i == 0){
              this.tabSousCategories[0].class = "categorie-niveau-1 categorie-Active"
              this.tabSousCategories[0].style = ""
            }else{
              this.tabSousCategories[i].class = "categorie-niveau-1"
              this.tabSousCategories[i].style = "display: none;"
            }
          
          }

        }
      })
     
      var styleName = ""
      var className = "categorie-niveau-1 categorie-Active"
      
      this.tabSousCategories.push({categorie:this.tabCategories[0], sousCategories: [] ,style:styleName, class:className})
      
      styleName = "display: none;"
      className = "categorie-niveau-1"
      
      for(var i = 1; i<this.tabCategories.length; i++ ){

         this.tabSousCategories.push({categorie:this.tabCategories[i], sousCategories: [] ,style:styleName, class:className})
      }
    
      this.tabTableau = this.tabCategories
      
    })
   
  }

  choisirCategorie(event){

    var marques = this.serv.tabMarques
    this.tabMarques = marques.filter(x=> (x.categorie == event.target.id))
    
    for (let i = 0; i <this.tabSousCategories.length; i++){
      if(this.tabSousCategories[i].categorie.id == event.target.id){
        this.tabSousCategories[i].class = "categorie-niveau-1 categorie-Active"
        this.tabSousCategories[i].style = ""
      }else{
        this.tabSousCategories[i].class = "categorie-niveau-1"
        this.tabSousCategories[i].style = "display:none;"
      }
    }

  }
  

  ngOnInit(): void {
   
  }

  getMarques(){
    this.isDisponible = false
    
    this.serv.listMarques().subscribe(res=>{
      this.isDisponible = true
      this.serv.tabMarques = res.resultat
      var marques = res.resultat
      this.tabMarques = marques.filter(x=> x.categorie == this.tabCategories[0].id )
    })

  }

  openModifiedMarque(event) {
    this.serv.idMarque = event.target.id
    const modalRef = this.modalService.open(ModefierMarqueComponent);
    modalRef.componentInstance.name = 'World';
  }

  openAddMarque() {
    const modalRef = this.modalService.open(AddMarqueComponent);
    modalRef.componentInstance.name = 'World';
  }

  openDeleteMarque(event) {
    this.serv.idMarque = event.target.id
    const modalRef = this.modalService.open(DeleteMarqueComponent);
    modalRef.componentInstance.name = 'World';
  }


}
