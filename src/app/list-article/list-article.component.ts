import { Component, OnInit ,ChangeDetectionStrategy, ChangeDetectorRef} from '@angular/core';
import { GestionArticleService } from '../service/gestion-article.service';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ArticleDetailComponent } from '../article-detail/article-detail.component';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { GestionCategorieService } from '../service/gestion-categorie.service';
declare var $: any;


@Component({
  selector: 'app-list-article',
  templateUrl: './list-article.component.html',
  styleUrls: ['./list-article.component.scss']
})

export class ListArticleComponent implements OnInit {
  tabA=[]
  search=""
  tabF: any[];
  
  formC:FormGroup
  
  tabCategoriesGlobal = []

  tabCategories = []
  
  tabCategories2 = []
  
  categorieParDefaut

  categorie
  
  categories={categorie:"0", sousCategorie:"0", sousSousCategorie:"0"}

  constructor(private serv:GestionArticleService,private cd: ChangeDetectorRef, private modalService: NgbModal,private fb:FormBuilder, private servCat:GestionCategorieService) { 
    this.formC=this.fb.group({
      search:['',[Validators.required]]
    })
  }

  ngOnInit(): void {
    this.servCat.listCategories().subscribe(res=>{
      
      this.servCat.setCategories(res.resultat)
      this.tabCategoriesGlobal = res.resultat
      this.servCat.tabCategories = this.tabCategoriesGlobal
     
      this.tabCategories = this.tabCategoriesGlobal.filter(x=> x.categorie == "0")
      if(this.tabCategories.length > 0){
        this.categorieParDefaut = this.tabCategories[0]
        this.categorie =  this.tabCategories[0].id
        this.categories={categorie:this.categorie, sousCategorie:"0", sousSousCategorie:"0"}

      }    
  
      this.tabCategories2 =  this.tabCategories.filter(x=> x.id != this.categorie)
  
    })

    this.serv.searchChange2.subscribe(res=>{
      this.search = res
      console.log(res)
    })

    this.tabA=this.serv.tabArticles
    /*this.serv.listArticle().subscribe(res=>{
      this.tabA=res.resultat
      this.tabF=this.tabA
    })
    */
  }
  


 /*change(cat){
    console.log(cat)
    this.tabF=this.tabA.filter(x=> x.categorie === cat)
    this.cd.detectChanges();
  }
  
  all(){
    this.serv.listArticle().subscribe(res=>{
      this.tabF=res.resultat
    })
  }
  */

  change(event){
    var cat = event.target.id
    var elements = document.getElementsByClassName("blogs")
   
    for (let i = 0; i <elements.length; i++){
      if(cat == elements[i].id){
        elements[i].setAttribute("style", "background-color:blue;")
      }else{
         elements[i].setAttribute("style", "")
      }
    }
  
    this.categorie = cat
    this.categories={categorie:this.categorie, sousCategorie:"0", sousSousCategorie:"0"}

  }

  openDetailsArticle(event) {
    this.serv.idArticle = event.target.id
    const modalRef = this.modalService.open(ArticleDetailComponent);
    modalRef.componentInstance.name = 'World';
  }

  recherche(){
   this.serv.setSearch2(this.formC.value.search)
  }

  
}
