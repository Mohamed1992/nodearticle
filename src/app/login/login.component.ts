import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { GestionUserService } from '../service/gestion-user.service';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { RegisterComponent } from '../register/register.component';

@Component({
  selector: 'ngbd-modal-content',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})

export class LoginComponent implements OnInit {
  formC:FormGroup
  message=""
  submitted=false
  isLoading=false

  constructor(private fb:FormBuilder, private router:Router,private serv:GestionUserService, public activeModal: NgbActiveModal, private modalService: NgbModal) {
   
    this.formC=this.fb.group({
      email:['',[Validators.required, Validators.minLength(1)]],
      password:['',[Validators.required, Validators.minLength(1)]]
    })
  }

  
  ngOnInit(): void {
  }

  login(){
    this.submitted=true
    
    if(this.controleInput(this.formC) && !this.isLoading){
      this.isLoading = true
      this.serv.login(this.formC.value).subscribe(res => {
        this.isLoading = false
        localStorage.setItem("Role",res.role)
        localStorage.setItem("Token",res.token)
        this.serv.loggIn()  
        this.serv.getRole()
        this.activeModal.close()
    //    this.router.navigate(['/profil'])
      }, err =>{
        this.isLoading = false
        alert(err._body)
      })
    }
  }


  controleInput(obj){

    console.log(obj.get("password").status)
    if(obj.get("password").status == "INVALID"){
      alert("SVP, inserer votre password")
      return false
    }else if(obj.get("email").status == "INVALID"){
      alert("SVP, inserer votre email")
      return false
    } 
    return true
  }

  register() {
    this.activeModal.close()
    this.serv.roleCompte = "client"
    const modalRef = this.modalService.open(RegisterComponent, {size:"lg"});
    modalRef.componentInstance.name = 'World';
  }
 
/*   
 if(this.formC.value.email == "admin@gmail.com" && this.formC.value.password == "123456"){
      var x = document.getElementsByClassName("modal-backdrop");
      alert("ce bon")
      x[0].removeAttribute('class')
      this.router.navigate(['/newArticle'])
      this.serv.loggIn(true)
    }else this.message="invalid mail or password"
    */
   
}
