import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { GestionUserService } from '../service/gestion-user.service';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  tabArticles
  formC:FormGroup
  selectedFile = null
  isBoutique = false
  formB:FormGroup
  withCommandes = false

  controleInput(obj){
    if(obj.get("email").status == "INVALID"){
      alert("SVP, inserer votre email")
      return false
    }else if(obj.get("password").status == "INVALID"){
      alert("SVP, inserer votre password")
      return false
    }else if(!this.isBoutique){
      if(obj.get("ville").status == "INVALID"){
        alert("SVP, inserer votre ville")
        return false
      }
    } 
    return true
  }

  constructor(private fb:FormBuilder,private serv:GestionUserService,  public activeModal: NgbActiveModal){
    this.formB=this.fb.group({
      nom:['',[Validators.required, Validators.min(1) ]],
      prenom:['',[Validators.required, Validators.min(1)]],
      password:['',[Validators.required, Validators.min(1)]],
      email:['', [Validators.required, Validators.min(1)]],   
      
        
      telephone:['',[Validators.required, Validators.min(1)]],
      NCarteIdentite:['',[Validators.required, Validators.min(1)]],
      adresse:['',[Validators.required, Validators.min(1)]],
      role:['',[Validators.required]],
      nomBoutique:['',[Validators.required, Validators.min(1)]],
      adressBoutique:['',[Validators.required, Validators.min(1)]],
      categorieBoutique:['Informatique',[Validators.required]],
      pays:['',[Validators.required, Validators.min(1)]],
      ville:['',[Validators.required, Validators.min(1)]],
    })

    this.formC=this.fb.group({
      nom:['',[Validators.required, Validators.min(1)]],
      prenom:['',[Validators.required, Validators.min(1)]],
      password:['',[Validators.required, Validators.min(1)]],
      email:['',[Validators.required, Validators.min(1)]],
      telephone:['',[Validators.required, Validators.min(1)]],
      NCarteIdentite:['',[Validators.required, Validators.min(1)]],
      adresse:['',[Validators.required, Validators.min(1)]],
      role:['',[Validators.required]],
      pays:['',[Validators.required, Validators.min(1)]],
      ville:['',[Validators.required, Validators.min(1)]],
    })
  }
  
  ngOnInit(){
    this.isBoutique = this.serv.roleCompte == "boutique"
  }

  ChangingValue(event){
    
    if(event.target.value == "boutique"){
       this.isBoutique = true 
    }else{
      this.isBoutique = false
    }
  
  }
  isLoading = false

  register(){
    
    this.formC.value.role = "client"
    if(this.controleInput(this.formC) && !this.isLoading){
      this.isLoading = true 
      this.serv.register(this.formC.value).subscribe(res => {
          
          if(res.status){
            this.isLoading = false

            console.log(res)
            localStorage.setItem("Role",res.role)
            localStorage.setItem("Token",res.token)
            this.serv.loggIn()  
            this.serv.getRole()

            alert("Félicitations ! Votre compte est activé.")
            
            this.serv.setLoading()
            this.activeModal.close()
           
          }
      },err => {
        this.isLoading = false
        alert(err._body)
      })
    }
  }

  changeRole(){
     this.withCommandes = !this.withCommandes
  }

  registerBoutique(data){
      
   

    if(this.formB.value.categorieBoutique == ""){
      this.formB.value.categorieBoutique = "Informatique"
    }

    if(!this.withCommandes){
      this.formB.value.role = "boutique"
    }else{
      this.formB.value.role = "boutiqueWithCommandes"
    }
    
    if(this.controleInput(this.formB) && !this.isLoading){
      this.isLoading = true
         
      this.serv.register(this.formB.value).subscribe(res => {
        if(res.status){
          this.isLoading = false
          alert("Félicitations ! Votre compte est activé.")
          
          this.serv.setLoading()
          this.formC.reset()
          this.activeModal.close()
        
        }else{
          this.isLoading = false
        
          alert("erreur d'ajouter")
        }
      },err =>{
        this.isLoading = false
        alert(err._body)
      })
    }
  }

  

}
