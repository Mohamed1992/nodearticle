import { Component, OnInit, Input } from '@angular/core';
import { WebSocketService } from '../service/web-socket.service';

@Component({
  selector: 'app-message-chat',
  templateUrl: './message-chat.component.html',
  styleUrls: ['./message-chat.component.scss']
})
export class MessageChatComponent implements OnInit {

  constructor(private serv:WebSocketService) {
   
   }

  @Input() message
  @Input() user
  isMan = true
  isWomen = false
  isAdmin=false
  className="style-message"
  ngOnInit(): void {
    if(this.message.recepteur != "admin"){
      this.className="style-message style-right"
      this.isAdmin = true
      this.isMan = false
    }else{
      if(this.user != null){
        if(this.user.sexe == 1){
          this.isWomen = true
          this.isMan = false
        }
      }
    }
    
    
  }

}
