import { Component, OnInit } from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { EventComponent } from '../event/event.component';
import { Router } from '@angular/router';
import { EventService } from '../service/event.service';
import { DeleteEventComponent } from '../delete-event/delete-event.component';

@Component({
  selector: 'app-events-component',
  templateUrl: './events-component.component.html',
  styleUrls: ['./events-component.component.scss']
})
export class EventsComponentComponent implements OnInit {

  isParametres = false
  tabEvents = []
  tabEvents2 = []
  
  constructor(private modalService: NgbModal, private router : Router, private serv:EventService,) { }

  ngOnInit(): void {
    this.getEvents()
    this.serv.eventsChange.subscribe(res=>{
      this.tabEvents = res
      this.getEvents()
    })

    this.isParametres = this.router.url == "/parametres"
    
    this.serv.listEvent().subscribe(res => {
      if(res.status){
        this.tabEvents = res.resultat
        this.getEvents()
        this.serv.setEvents(this.tabEvents)
      }
    })  
  }

  getEvents(){
        this.tabEvents2 = []

        var className = "item active"
        
        var styleNameLeft = "padding: 5px; font-size: 12px; float:right !important; margin:0px !important; position:absolute; left:0px; top:0px;"
        var styleNameCenter = "padding: 5px; font-size: 12px; float:right !important; margin:0px !important; position:absolute; right:0px; top:0px;"
        var styleNameRight = "padding: 5px; font-size: 12px; float:right !important; margin:0px !important; position:absolute; right:0px; top:0px;"

        for(var i=0; i < this.tabEvents.length; i+=3){
          if(i != 0){
            className = "item"
          }
          
          if(i+2 <  this.tabEvents.length){
            this.tabEvents2.push({tabs:[{event:this.tabEvents[i], style:styleNameRight}, {event: this.tabEvents[i+1] , style:styleNameCenter}, {event:this.tabEvents[i+2] , style:styleNameLeft}], class:className})
          }else if(i+1 <  this.tabEvents.length){
            this.tabEvents2.push({tabs:[{event:this.tabEvents[i], style:styleNameRight}, {event: this.tabEvents[i+1] , style:styleNameCenter}], class:className})
          }else{
            this.tabEvents2.push({tabs:[{event:this.tabEvents[i], style:styleNameRight}], class:className})
          }
          
        }
        
  }

  openAjouterEvent() {
    const modalRef = this.modalService.open(EventComponent);
    modalRef.componentInstance.name = 'World';
  }

  openDeleteEvent(event) {
    this.serv.eventId = event.target.id
    const modalRef = this.modalService.open(DeleteEventComponent);
    modalRef.componentInstance.name = 'World';
  }

}
