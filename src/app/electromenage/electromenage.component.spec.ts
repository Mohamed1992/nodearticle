import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ElectromenageComponent } from './electromenage.component';

describe('ElectromenageComponent', () => {
  let component: ElectromenageComponent;
  let fixture: ComponentFixture<ElectromenageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ElectromenageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ElectromenageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
