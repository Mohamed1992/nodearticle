import { Component, OnInit } from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { GestionCategorieService } from '../service/gestion-categorie.service';
import { GestionArticleService } from '../service/gestion-article.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
declare var $: any;

@Component({
  selector: 'app-add-categorie',
  templateUrl: './add-categorie.component.html',
  styleUrls: ['./add-categorie.component.scss']
})

export class AddCategorieComponent implements OnInit {

  formC:FormGroup
  
  isLoading = false

  idCategorie = '1'
  tabCategoriesGlobal = []

  selectedFile = null

  tabCategories = []

  tabSousCategories = []

  tabSousSousCategories = []

 

  constructor(private fb:FormBuilder, public activeModal: NgbActiveModal, private serv:GestionCategorieService, private serv2:GestionArticleService) {
    this.serv.logchange.subscribe(res=>{
     
      this.tabCategoriesGlobal = res
   
      this.tabCategories = this.tabCategoriesGlobal.filter(x=> x.categorie == "0")  

      for(var i = 0; i<this.tabCategories.length; i++ ){
         this.tabSousCategories.push({categorie:this.tabCategories[i].id, sousCategories: this.tabCategoriesGlobal.filter(x=> x.categorie == this.tabCategories[i].id) })
      }

    })

    this.formC=this.fb.group({
      nom:['',[Validators.required]],
      categorie:['0',[Validators.required]],
      sousCategorie:['',[Validators.required]],
    })
   }


 
  ngOnInit(): void {
  }

  addCategorie(){
    let categorie
    let nom
    
    if(this.formC.value.categorie == ''){
      this.formC.value.categorie = '0'
      nom = this.formC.value.nom
      categorie = this.formC.value.categorie
    }else{
      if(this.formC.value.sousCategorie == '0' || this.formC.value.sousCategorie == ''){
          nom = this.formC.value.nom
          categorie = this.formC.value.categorie
      }else{
  
        let listCats = this.tabCategoriesGlobal
        listCats = listCats.filter(x=> x.id == this.formC.value.sousCategorie)
        
        if(listCats[0].categorie != this.formC.value.categorie ){
          nom = this.formC.value.nom,
          categorie = this.formC.value.categorie
        }else{
          nom = this.formC.value.nom,
          categorie = this.formC.value.sousCategorie
        }
      }
    }
   
    if(this.formC.value.nom == ""){
      alert("SVP inserer le nom")
    }else{
        if(!this.isLoading){
          var request = {nom:nom, categorie:categorie }
          this.isLoading = true
    
          this.serv.newCategorie(request).subscribe(res => {
            if(res.status){
              this.isLoading = false
          
              alert("categorie ajouter")
              this.activeModal.close()
              this.serv.setLoading()
            }
          }, err =>{
            this.isLoading = false
            alert(err._body)
            
          })

      }
        
    }

  }

  changeCategorie(event){
    
    this.formC.value.sousCategorie == ''
    if(event.target.value !=''){
      this.idCategorie =event.target.value
    }

     
    var elements = document.getElementsByClassName("sous-categorie-22")

    for (let i = 0; i <elements.length; i++){
      $("#"+elements[i].id).val(0);
      if(elements[i].id == this.idCategorie){
         elements[i].setAttribute("style", "  ")
      }else{
        elements[i].setAttribute("style", "visibility: hidden;")
      }
    }

  }

}

