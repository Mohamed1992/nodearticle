import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { GestionUserService } from '../service/gestion-user.service';
import { ActivatedRoute, Router } from '@angular/router';
declare var $: any;

@Component({
  selector: 'app-profil',
  templateUrl: './profil.component.html',
  styleUrls: ['./profil.component.scss']
})
export class ProfilComponent implements OnInit {

  formC:FormGroup
  selectedFile = null
  isBoutique = false
  formB:FormGroup
  withCommandes = false 
  
  constructor(private fb:FormBuilder,private serv:GestionUserService, private route2: Router){
    this.formB=this.fb.group({
      nom:['',[Validators.required]],
      prenom:['',[Validators.required]],
      password:['',[Validators.required]],
      newPassword:['',[Validators.required]],
      email:['',[Validators.required]],
      telephone:['',[Validators.required]],
      NCarteIdentite:['',[Validators.required]],
      adresse:['',[Validators.required]],
      nomBoutique:['',[Validators.required]],
      adressBoutique:['',[Validators.required]],
      categorieBoutique:['',[Validators.required]],
      ville:[''],
      pays:[''],
    })

    this.formC=this.fb.group({
      nom:['',[Validators.required]],
      prenom:['',[Validators.required]],
      password:['',[Validators.required]],
      newPassword:['',[Validators.required]],
      email:['',[Validators.required]],
      telephone:['',[Validators.required]],
      NCarteIdentite:['',[Validators.required]],
      adresse:['',[Validators.required]],
      ville:[''],
      pays:[''],
  
    })
  }
  
  ngOnInit(){
    this.isBoutique = localStorage.getItem("Role") != "client" && localStorage.getItem("Role") != "admin"
   
      this.serv.monCompte().subscribe(res => {
        if(res.status){
          if(this.isBoutique){
            this.formB=this.fb.group({
              nom:[res.resultat.nom,[Validators.required, Validators.minLength(1)]],
              prenom:[res.resultat.prenom,[Validators.required, Validators.minLength(1)]],
              password:['',[Validators.required, Validators.minLength(1)]],
              newPassword:['',[Validators.required, Validators.minLength(1)]],
              email:[res.resultat.email,[Validators.required, Validators.minLength(1)]],
              telephone:[res.resultat.telephone,[Validators.required, Validators.minLength(1)]],
              NCarteIdentite:[res.resultat.NCarteIdentite,[Validators.required, Validators.minLength(1)]],
              adresse:[res.resultat.adresse,[Validators.required, Validators.minLength(1)]],
              nomBoutique:[res.resultat.nomBoutique,[Validators.required, Validators.minLength(1)]],
              adressBoutique:[res.resultat.adressBoutique,[Validators.required, Validators.minLength(1)]],
              categorieBoutique:[res.resultat.categorieBoutique,[Validators.required, Validators.minLength(1)]],
              ville:[res.resultat.ville,[Validators.required, Validators.minLength(1)]],
              pays:[res.resultat.pays,[Validators.required, Validators.minLength(1)]],
          
             })
          }else{
            this.formC=this.fb.group({
              nom:[res.resultat.nom,[Validators.required, Validators.minLength(1)]],
              prenom:[res.resultat.prenom,[Validators.required, Validators.minLength(1)]],
              password:['',[Validators.required, Validators.minLength(1)]],
              newPassword:['',[Validators.required, Validators.minLength(1)]],
              email:[res.resultat.email,[Validators.required, Validators.minLength(1)]],
              telephone:[res.resultat.telephone,[Validators.required, Validators.minLength(1)]],
              NCarteIdentite:[res.resultat.NCarteIdentite,[Validators.required, Validators.minLength(1)]],
              adresse:[res.resultat.adresse,[Validators.required, Validators.minLength(1)]],
              ville:[res.resultat.ville,[Validators.required, Validators.minLength(1)]],
              pays:[res.resultat.pays,[Validators.required, Validators.minLength(1)]],
            
             })

          }
        }
      })

  }

  controleInput(obj){
  
    if(obj.get("email").status == "INVALID"){
      alert("SVP, inserer votre email")
      return false
    }else if(obj.get("password").status == "INVALID"){
      alert("SVP, inserer votre password")
      return false
    }else if(!this.isBoutique){
      if(obj.get("ville").status == "INVALID"){
        alert("SVP, inserer votre ville")
        return false
      }
    } 
    
    
    return true
  }

  ChangingValue(event){
    if(event.target.value == "boutique"){
       this.isBoutique = true 
    }else{
      this.isBoutique = false
    }
  }
isLoading = false

update(){
    if(this.controleInput(this.formC) && !this.isLoading){
      this.isLoading = true
      this.serv.UpdateCompte(this.formC.value).subscribe(res => {
        if(res.status){
          this.isLoading = false
          alert("Votre compte est modifié avec succès")
          $("html, body").animate({scrollTop : 0},700);
          this.serv.logout()
          this.route2.navigate(['/accueil'])
        
        }else{
          this.isLoading = false
          alert("Verifier votre mot de passe")
        }
        },err =>{
        alert(err._body)
        this.isLoading = false
      })
    }
  }

  
  changeRole(){
     this.withCommandes = !this.withCommandes
  }

  updateBoutique(){
      

    if(this.controleInput(this.formB) && !this.isLoading){
      this.isLoading = true
      this.serv.UpdateCompte(this.formB.value).subscribe(res => {
        if(res.status){
            this.isLoading = false
            alert("Votre compte est modifié avec succès")
            $("html, body").animate({scrollTop : 0},700);
            this.serv.logout()
            this.route2.navigate(['/accueil'])
          
          }else{
            this.isLoading = false
            alert("Verifier votre mot de passe")
          }
          },err =>{
          alert(err._body)
          this.isLoading = false
        })
      }
  }
  
  

}
