import { Component, OnInit } from '@angular/core';
import { GestionArticleService } from '../service/gestion-article.service';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CommandeDetailsComponent } from '../commande-details/commande-details.component';

@Component({
  selector: 'app-commandes',
  templateUrl: './commandes.component.html',
  styleUrls: ['./commandes.component.scss']
})
export class CommandesComponent implements OnInit {

  constructor(private serv:GestionArticleService, private modalService: NgbModal) {
    this.serv.loadingCommandes.subscribe(res =>{
      this.getCommandes(this.etat)
    })  

    this.serv.tabCommandes.subscribe(res=>{
      this.tabCommandes = res
    })
  }
  
  charge = false
  tabCommandes
  etats = ["enAttent","accepte","refuse","fini"]
  isAdmin = false
  page=1
  totalPage = 1
  etat = "enAttent"

  ngOnInit(): void {
    this.isAdmin = localStorage.getItem("Role") == "admin"
  }

  getCommandes(etat){
    this.charge = true
    let request = {page:this.page}
    this.serv.listCommandes(etat,request).subscribe(res=>{
      this.charge = false
      this.tabCommandes = res.resultat.docs
      for(let i = 0; i < this.tabCommandes.length; i++){
        this.tabCommandes[i].isShow = this.tabCommandes[i].isShowAdmin == 0 
      }
      this.serv.setCommandes(this.tabCommandes)
      this.totalPage = res.resultat.pages
      this.page = res.resultat.page

    })
  }

  clickEtat(event){
    var elements = document.getElementsByClassName("button-etat")
    this.etat = event.target.id    
    this.getCommandes(event.target.id)

    for (let i = 0; i <elements.length; i++){
      if(elements[i].id == event.target.id){
        elements[i].setAttribute("style", "padding: 10px; font-size: 15px;")
       }else{
        elements[i].setAttribute("style", "padding: 5px; font-size: 12px;")
     
      }
    }

  }

  openDetailsCommande(event) {
    this.serv.idCommande = event.target.id
    const modalRef = this.modalService.open(CommandeDetailsComponent, {size:"lg"});
    modalRef.componentInstance.name = 'World';
  }


  setPage(newPage: number) {
    this.page = newPage
    this.getCommandes(this.etat)
  }

}
