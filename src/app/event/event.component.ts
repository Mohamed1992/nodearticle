import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { EventService } from '../service/event.service';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-event',
  templateUrl: './event.component.html',
  styleUrls: ['./event.component.scss']
})
export class EventComponent implements OnInit {
  formE:FormGroup

  public imagePath;
  imgURL: any;
  public message: string;
  
  constructor(private serv:EventService,private Fb:FormBuilder, public activeModal: NgbActiveModal) { }
  
  ngOnInit() {
    this.formE=this.Fb.group({
     titre:['',[Validators.required]],
     description:['',[Validators.required]],
     image:['',[Validators.required]],
    })

  }
  

  multiImage
  selectedM(event) {
     this.multiImage = event.target.files;
     var files = event.target.files;
     if (files.length === 0)
     return;

     var mimeType = files[0].type;
     if (mimeType.match(/image\/*/) == null) {
       this.message = "Only images are supported.";
       return;
     }

     var reader = new FileReader();
     this.imagePath = files;
     reader.readAsDataURL(files[0]); 
     reader.onload = (_event) => { 
       this.imgURL = reader.result; 
     }

  }

  ajouterEvent(){
   const formData = new FormData();
    for (let img of this.multiImage)
      formData.append('myFiles', img)
      this.serv.createImg(formData).subscribe(res => {

      let obj = this.formE.value
      obj.image = res[0]
     
      this.serv.newEvent(obj).subscribe(res => {
        if(res.status){
         const events =this.serv.varEvents
         events.push(res.resultat)
         this.serv.setEvents(events)
          alert("event ajouter")
        //  this.formC.reset()
        }
      })
    })
    this.activeModal.close()
  }

}

