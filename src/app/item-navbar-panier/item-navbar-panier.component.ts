import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-item-navbar-panier',
  templateUrl: './item-navbar-panier.component.html',
  styleUrls: ['./item-navbar-panier.component.scss']
})
export class ItemNavbarPanierComponent implements OnInit {

  @Input() item

  isPromo = false
  constructor() { }

  ngOnInit(): void {
    if(this.item.article.isPromo == "1"){
      this.isPromo = true
    }else{
      this.isPromo = false
    }
  }

}
