import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemNavbarPanierComponent } from './item-navbar-panier.component';

describe('ItemNavbarPanierComponent', () => {
  let component: ItemNavbarPanierComponent;
  let fixture: ComponentFixture<ItemNavbarPanierComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemNavbarPanierComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemNavbarPanierComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
