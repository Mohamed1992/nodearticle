import { Component, OnInit } from '@angular/core';
import { GestionCategorieService } from '../service/gestion-categorie.service';
import { GestionUserService } from '../service/gestion-user.service';
import { Router, NavigationEnd } from '@angular/router';
declare var $:any;

@Component({
  selector: 'app-navbar-site',
  templateUrl: './navbar-site.component.html',
  styleUrls: ['./navbar-site.component.scss']
})
export class NavbarSiteComponent implements OnInit {

  tabRoutesDashboard = ["/parametres", "/categories", "/profil", "/commandes", "/clients", "/boutiques", "/marques", "/articles", "/newArticle"]
  
  constructor(private serv:GestionCategorieService, private serv2:GestionUserService, private router:Router) {
    
    this.serv2.roleChange.subscribe(res =>{ 
      
      if(res == "client"){
        this.isClient = true
        this.isAdmin = false
        this.isBoutique = false
        this.isBoutiqueWithCommande = false
      }else if(res == "admin"){
        this.isClient = false
        this.isAdmin = true
        this.isBoutique = false
        this.isBoutiqueWithCommande = false
      }else if(res == "boutique"){
        this.isClient = false
        this.isAdmin = false
        this.isBoutique = true
        this.isBoutiqueWithCommande = false
      }else if(res == "boutiqueWithCommandes"){
        this.isClient = false
        this.isAdmin = false
        this.isBoutique = false
        this.isBoutiqueWithCommande = true
      }
    })

      router.events.subscribe((val) => {
        // see also 
        if(val instanceof NavigationEnd){
          var ok = false
          for(var i=0; i<this.tabRoutesDashboard.length;i++ ){
               if(this.tabRoutesDashboard[i] == val.url){
                 ok=true
               }
          }
          this.isDashboard = ok
        } 
    });

    
  }

  isDashboard = false 

  tabCategoriesGlobal = []

  tabCategories = []

  isShowNavbar = false  

  isAdmin = false
  isClient = false
  isBoutique = false
  isBoutiqueWithCommande = false
    
  ngOnInit(): void {
    
   /* $(document).ready(function(){
      $(".open-dropdown").hover(function(){
        $(this).addClass("open");
      });

      $(".style-ul-2").mouseout(function(){
        $(".open-dropdown").removeClass("open");
      });
      
    });
   */
    if(this.serv.tabCategories.length > 0){
      this.tabCategoriesGlobal = this.serv.tabCategories 
      
    }else{
      this.serv.listCategories().subscribe(res=>{
        this.serv.setCategories(res.resultat)
        this.tabCategoriesGlobal = res.resultat
        this.serv.tabCategories = this.tabCategoriesGlobal
      })
    }
    
  }

  

  goPageArticles(event){
     
     var categorieFilter = this.tabCategoriesGlobal.filter(x=> x.nom == event.target.id)
     if(categorieFilter.length > 0){
       var categorie = categorieFilter[0]
       this.serv.setCategorieFiltration({categorie:categorie.id, sousCategorie: "0", sousSousCategorie: "0"}) 
       this.router.navigate(['/'+event.target.id])
       this.dessondre()
     }

     this.showNavbar()
  }

  goSousCategorie(event){
    console.log(event.target.id);
    var sousCategorieFilter = this.tabCategoriesGlobal.filter(x=> x.nom == event.target.id)
    if(sousCategorieFilter.length > 0){
       var sousCategorie = sousCategorieFilter[0]
       var categorie = this.tabCategoriesGlobal.filter(x=> x.id == sousCategorie.categorie)[0]
       this.serv.setCategorieFiltration({categorie:categorie.id, sousCategorie: sousCategorie.id, sousSousCategorie: "0"}) 
       this.router.navigate(['/'+categorie.nom])
       this.dessondre()   
    }
    this.showNavbar()
  }

  goSousSousCategorie(event){
    var sousSousCategorieFilter = this.tabCategoriesGlobal.filter(x=> x.nom == event.target.id)
     
    if(sousSousCategorieFilter.length > 0){
       var sousSousCategorie = sousSousCategorieFilter[0]
       var sousCategorie = this.tabCategoriesGlobal.filter(x=> x.id == sousSousCategorie.categorie)[0]
       var categorie = this.tabCategoriesGlobal.filter(x=> x.id == sousCategorie.categorie)[0]
       
       this.serv.setCategorieFiltration({categorie:categorie.id, sousCategorie: sousCategorie.id, sousSousCategorie: sousSousCategorie.id}) 
       this.router.navigate(['/'+categorie.nom])
       this.dessondre()
    }
    this.showNavbar()
  }

dessondre(){
  let produits = document.getElementsByClassName('startArticles')

  if(produits.length == 0){
     return
  }

  produits[0].scrollIntoView({
    behavior: "smooth",
    block: "center",
    inline: "end"
  })
}

showNavbar(){

 var navbar = document.getElementsByClassName("menu")[0]
 if(!this.isShowNavbar){
     navbar.setAttribute("class", "menu active")
     this.isShowNavbar = true
 }else{
     navbar.setAttribute("class", "menu")
     this.isShowNavbar = false
 }
}

/*  setCategorie(){

      for(var i = 0; i<this.tabCategories.length; i++ ){
        var listSousCategories = this.tabCategoriesGlobal.filter(x=> x.categorie == this.tabCategories[i].id)
        

        var sousCategoriesWithSousSous = []
        
        for(var j = 0; j<listSousCategories.length; j++ ){
          var sousSousCategories = this.tabCategoriesGlobal.filter(x=> x.categorie == listSousCategories[j].id)
          if(sousSousCategories.length > 0){
            sousCategoriesWithSousSous.push({sousCategorie: listSousCategories[j],class:"active-navbar", active: true , sousSousCategories: sousSousCategories })
          }else{
            sousCategoriesWithSousSous.push({sousCategorie: listSousCategories[j],class:"" ,active: false , sousSousCategories: [] })
          }
        }

        if(sousCategoriesWithSousSous.length > 0){
            this.tabSousCategories.push({categorie:this.tabCategories[i], class:"active-navbar" , active: true, sousCategories:  sousCategoriesWithSousSous})
        }else{
            this.tabSousCategories.push({categorie:this.tabCategories[i], class:"", active: false, sousCategories:  []})
        }

      }

  }
*/
}
