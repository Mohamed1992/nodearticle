

import { Component, OnInit } from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { GestionUserService } from '../service/gestion-user.service';


@Component({
  selector: 'app-details-user',
  templateUrl: './details-user.component.html',
  styleUrls: ['./details-user.component.scss']
})
export class DetailsUserComponent implements OnInit {

user={nom:"hed", prenom:"jiji", nomBoutique:"", telephone:"",isEnabled:"0", email:"" ,createdAt:"", adresse:"", NCarteIdentite:"",categorieBoutique:"",adressBoutique:"",num:0}
user2
isBoutique = false
isClient = false
isEnabled = false
categorieBoutique = "informatique"

  constructor(public activeModal: NgbActiveModal, private serv:GestionUserService,) { }

  ngOnInit(): void {
    
    this.serv.userById().subscribe(res=>{
      this.user2=res.resultat
      if(this.user2.role == "boutique" || this.user2.role == "boutiqueWithCommandes"){
        this.isBoutique = true
        this.categorieBoutique = this.user2.categorieBoutique
      }else{
        this.isClient = true
      }
      
      this.user = this.user2
      this.isEnabled = this.user2.isEnabled == "1" ? true : false
     
    })
  }

  

  activeOrDesactive(){
    var isEnabled = this.isEnabled  ? "0" : "1"
   
    this.serv.activeOrDesactive(isEnabled).subscribe(res=>{
      this.activeModal.close()
    })
  }

}
