import { Component, OnInit } from '@angular/core';
import { GestionArticleService } from '../service/gestion-article.service';
import { GestionCategorieService } from '../service/gestion-categorie.service';

import { ActivatedRoute } from '@angular/router';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, Validators,FormControl } from '@angular/forms';
import { Router } from '@angular/router';
declare var $: any;

@Component({
  selector: 'app-article-update',
  templateUrl: './article-update.component.html',
  styleUrls: ['./article-update.component.scss']
})
export class ArticleUpdateComponent implements OnInit {
  formC:FormGroup
  article
  
  isPromo=false
  isPopulaires=false
  isFacilite=false
  isTopCategorie = false
  
  public imagePath;
  imgURL: any;
  public message: string;
  
  isAdmin

  idSousCategorie = '0'

  idCategorie = '1'

  tabCategoriesGlobal = []

  tabCategories = []

  tabSousCategories = []

  isLoading = false

  isBoutiqueWithCommande = false

  constructor(private fb:FormBuilder, private route:ActivatedRoute,private serv:GestionArticleService, public activeModal: NgbActiveModal, private router : Router, private servCat:GestionCategorieService) {
    this.isAdmin = localStorage.getItem("Role") == "admin" 
    this.isBoutiqueWithCommande = localStorage.getItem("Role") == "boutiqueWithCommandes"

    this.formC=this.fb.group({
      nom:['',[Validators.required]],
      prix:['',[Validators.required]],
      ref:['',[Validators.required]],
      qtn:['',[Validators.required]],
      description:['',[Validators.required]],
      newPrix:[0,[Validators.required]],
      pourcentageRemise:[0,[Validators.required]],
      dateDebut:['',[Validators.required]],
      dateFin:['',[Validators.required]],
      isPopulaires:['0',[Validators.required]],
      isTopCategorie:['0',[Validators.required]],
      isFacilite:['0',[Validators.required]],
      threeMonthe:['0',[Validators.required]],
      sexMonthe:['0',[Validators.required]],
      nineMonthe:['0',[Validators.required]],
      tweleveMonthe:['0',[Validators.required]],
      categorie:['0',[Validators.required]],
      sousCategorie:['0',[Validators.required]],
      sousSousCategorie:['0',[Validators.required]],
    
    })

    this.servCat.listCategories().subscribe(res=>{
      
      this.servCat.setCategories(res.resultat)

      this.tabCategoriesGlobal = res.resultat
      
      this.servCat.tabCategories = this.tabCategoriesGlobal
     
      this.tabCategories = this.tabCategoriesGlobal.filter(x=> x.categorie == "0")  

      for(var i = 0; i<this.tabCategories.length; i++ ){
        var listSousCategories = this.tabCategoriesGlobal.filter(x=> x.categorie == this.tabCategories[i].id)
        
        var sousCategoriesWithSousSous = []
        
        for(var j = 0; j<listSousCategories.length; j++ ){
          sousCategoriesWithSousSous.push({sousCategorie: listSousCategories[j], sousSousCategories: this.tabCategoriesGlobal.filter(x=> x.categorie == listSousCategories[j].id) })
        }

        this.tabSousCategories.push({categorie:this.tabCategories[i], sousCategories:  sousCategoriesWithSousSous })
      }

    })
  }


  ngOnInit(): void {
    this.isLoading = true
    this.serv.articleById().subscribe(res=>{
      this.article=res.resultat
      this.isLoading = false
 
  
      this.isTopCategorie = this.article.isTopCategorie == "1"
      this.isPromo = this.article.isPromo == "1"
      this.isPopulaires = this.article.isPopulaires == "1"
      this.isFacilite = this.article.isFacilite == "1"
      this.imgURL = this.article.image

      this.formC=this.fb.group({
        nom:[this.article.nom,[Validators.required]],
        prix:[this.article.prix,[Validators.required]],
        ref:[this.article.ref,[Validators.required]],
        qtn:[this.article.qtn,[Validators.required]],
        description:[this.article.description,[Validators.required]],
        newPrix:[this.article.newPrix,[Validators.required]],
        pourcentageRemise:[this.article.pourcentageRemise,[Validators.required]],
        dateDebut:[this.article.dateDebut,[Validators.required]],
        dateFin:[this.article.dateFin,[Validators.required]],
        isPopulaires:['0',[Validators.required]],
        isTopCategorie:['0',[Validators.required]],
        isFacilite:['0',[Validators.required]],
        threeMonthe:[this.article.threeMonthe,[Validators.required]],
        sexMonthe:[this.article.sexMonthe,[Validators.required]],
        nineMonthe:[this.article.nineMonthe,[Validators.required]],
        tweleveMonthe:[this.article.tweleveMonthe,[Validators.required]],
        categorie:['0', [Validators.required]],
        sousCategorie:['0', [Validators.required]],
        sousSousCategorie:['0', [Validators.required]],
        marque:[this.article.marque,[Validators.required]],
      
      }) 
    })

  }

  

  multiImage
  selectedM(event) {
     this.multiImage = event.target.files;
     var files = event.target.files;
     if (files.length === 0)
     return;

     var mimeType = files[0].type;
     if (mimeType.match(/image\/*/) == null) {
       this.message = "Only images are supported.";
       return;
     }

     var reader = new FileReader();
     this.imagePath = files;
     reader.readAsDataURL(files[0]); 
     reader.onload = (_event) => { 
       this.imgURL = reader.result; 
     }

  }
  
  changePromotion(){
    this.isPromo = !this.isPromo
  }

  changeFacilite(){
    this.isFacilite = !this.isFacilite
  }
  
  changePopulaires(){
    this.isPopulaires = !this.isPopulaires
  }

  changeTopCategorie(){
    this.isTopCategorie = !this.isTopCategorie
  }

  modified(){

    if(this.isLoading){
      return false
    }

    if(this.isPopulaires){
      this.formC.value.isPopulaires = "1"
    }else{
      this.formC.value.isPopulaires = "0"
    }

    if(this.isFacilite){
      this.formC.value.isFacilite = "1"
    }else{
      this.formC.value.isFacilite = "0"
    }

    if(!this.isPromo){
        this.formC.value.newPrix = 0
        this.formC.value.pourcentageRemise = 0
        this.formC.value.dateDebut = "00"
        this.formC.value.dateFin = "00"
    }else{
       if(this.formC.value.newPrix == 0){
          alert("Le prix de promotion n'est pas valide")
          return false      
       }else if(this.formC.value.pourcentageRemise == 0){
          alert("Le pourcentage de promotion n'est pas valide")
          return false      
       }else if(this.formC.value.dateDebut == "00"){
          alert("Le debut de promotion n'est pas valide")
          return false      
       }else if(this.formC.value.dateFin == "00"){
          alert("Le fin de promotion n'est pas valide")
          return false      
       }
    }

    if(!this.controleInput(this.formC.value)){
         return false
    }
    
    this.isLoading = true

    this.formC.value.image = this.imgURL
    if(this.isTopCategorie){
        this.formC.value.isTopCategorie = "1"
    }
       
    if(this.multiImage){
          this.isLoading = true
      
          const formData = new FormData();
          for (let img of this.multiImage)
            formData.append('myFiles', img)
            
            this.serv.createImg(formData).subscribe(res => {
      
            let obj = this.formC.value
            obj.image = res[0]
            
            if(obj.categorie == "0"){
              obj.categorie = this.article.categorie
              obj.sousCategorie = this.article.sousCategorie
              obj.sousSousCategorie = this.article.sousSousCategorie
            }
              
            this.serv.modifiedArticle(obj).subscribe(res=>{
                if(this.router.url != "/parametres"){
                  this.serv.setArticle2(this.formC.value,this.isPromo)
                } 
                this.isLoading = false
                this.serv.setLoading()
                alert("l'article est modifier")
                this.activeModal.close()
              },err =>{
                this.isLoading = false
                alert("erreur")
              })
          },err =>{
            this.isLoading = false
            alert("erreur de copier image")
          })
        
    }else{
        
          if(this.formC.value.categorie == "0"){
            this.formC.value.categorie = this.article.categorie
            this.formC.value.sousCategorie = this.article.sousCategorie
            this.formC.value.sousSousCategorie = this.article.sousSousCategorie
          }
          this.isLoading = true
          this.serv.modifiedArticle(this.formC.value).subscribe(res=>{
            this.isLoading = false
            if(this.router.url != "/parametres"){
              this.serv.setArticle2(this.formC.value,this.isPromo)
            } 
            this.serv.setLoading()  
            alert("l'article est modifier")
            this.activeModal.close()
          },err =>{
            this.isLoading = false
            alert("erreur")
          })
    }  

  }

  controleInput(obj){
    if(obj.nom == ""){
      alert("SVP, inserer le nom") 
      return false
    }else if(obj.prix == 0){
      alert("SVP, inserer le prix") 
      return false
    }else if(this.isPromo){
      if(obj.newPrix == 0){
        alert("SVP, inserer nouveau prix") 
        return false
      }else if(obj.pourcentageRemise == 0){
        alert("SVP, inserer le pourcentage de remise") 
        return false
      }
    } 
    return true
  }

  
  
  changeCategorie(event){
    this.formC.value.sousCategorie == ''
    
    if(event.target.value !=''){
      this.idCategorie =event.target.value
    }
    
   // this.setMarques(this.idCategorie,"0","0")

    var elements = document.getElementsByClassName("sous-categorie-22")
   // var marques = document.getElementsByClassName("marques-22")
    
    //marques[0].setAttribute("style", "visibility: hidden;")
    //marques[1].setAttribute("style", "")
    
    for (let i = 0; i <elements.length; i++){
      
      if(elements[i].id == this.idCategorie){
         elements[i].setAttribute("style", "  ")
    
      }else{
        elements[i].setAttribute("style", "visibility: hidden;")
      }
    }

    this.initializeSousSousCategorie()
    this.initializeSousCategorie()
//    this.initializeMarques()

    var sousElements = document.getElementsByClassName("sous-categorie-33")
   
  

    for (let i = 0; i <sousElements.length; i++){
      
      if(sousElements[i].id == "1000"){
        sousElements[i].setAttribute("style", "  ")
      }else{
        sousElements[i].setAttribute("style", "visibility: hidden;")
      }
    }

  }

  changeSousCategorie(event){
    if(event.target.value !=''){
      this.idSousCategorie =event.target.value
    }
     
   // this.setMarques("0",this.idSousCategorie,"0")

    var elements = document.getElementsByClassName("sous-categorie-33")
   
  

    for (let i = 0; i <elements.length; i++){
      
      if(elements[i].id == this.idSousCategorie){
         elements[i].setAttribute("style", "  ")
      }else{
        elements[i].setAttribute("style", "visibility: hidden;")
      }
    }

    this.initializeSousSousCategorie()

  }

  initializeSousCategorie(){
    var elements = document.getElementsByClassName("sous-categorie-22")
   
    for (let i = 0; i <elements.length; i++){
      $('#'+elements[i].id).val(0) 
    }
    this.formC.value.sousCategorie = "0"
    
  }

 /* initializeMarques(){
    var elements = document.getElementsByClassName("marques-22")
   
    for (let i = 0; i <elements.length; i++){
      $('#'+elements[i].id).val(0) 
    }

  }
*/
  initializeSousSousCategorie(){
    var elements = document.getElementsByClassName("sous-categorie-33")
   
    for (let i = 0; i <elements.length; i++){
      $('#'+elements[i].id).val(0) 
    }
    this.formC.value.sousSousCategorie = "0"
  }

  changeSousSousCategorie(event){
//    this.setMarques("0","0", event.target.value)
  }

}
