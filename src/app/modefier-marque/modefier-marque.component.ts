import { Component, OnInit } from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { GestionCategorieService } from '../service/gestion-categorie.service';
import { GestionMarquesService } from '../service/gestion-marques.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
declare var $: any;

@Component({
  selector: 'app-modefier-marque',
  templateUrl: './modefier-marque.component.html',
  styleUrls: ['./modefier-marque.component.scss']
})

export class ModefierMarqueComponent implements OnInit {
isLoading = false
  formC:FormGroup
  
  
  
  marque

  

  constructor(private fb:FormBuilder, public activeModal: NgbActiveModal, private serv:GestionCategorieService, private serv2:GestionMarquesService) {
    
  

    var marques = this.serv2.tabMarques.filter(x => x.id == this.serv2.idMarque)
    
    if(marques.length > 0){
      this.marque = marques[0]
    }
    
    this.formC=this.fb.group({
      nom:[this.marque.nom,[Validators.required]],
    })

    //$('#00').val(this.marque.categorie.id)
  }

  ngOnInit(): void {
  }

  updateMarque(){
    
    
    if(this.formC.value.nom != "" && !this.isLoading){
      this.isLoading = true
      this.serv2.updateMarque(this.marque.id, this.formC.value).subscribe(res => {
        if(res.status){
          this.isLoading = false
          alert("marque modifier")
          this.activeModal.close()
          this.serv2.setLoading()
        }
      }, err =>{
        this.isLoading = false
        alert("erreur")
        
      })
   }else{
    this.isLoading = false
     alert("SVP inserer le nom")
   } 
    
  }

}
