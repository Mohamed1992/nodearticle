import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModefierMarqueComponent } from './modefier-marque.component';

describe('ModefierMarqueComponent', () => {
  let component: ModefierMarqueComponent;
  let fixture: ComponentFixture<ModefierMarqueComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModefierMarqueComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModefierMarqueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
