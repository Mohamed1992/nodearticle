import { Component, OnInit, Input, SimpleChanges , Output, EventEmitter } from '@angular/core';
declare var $: any;
import { FormBuilder, FormGroup, Validators } from '@angular/forms';


@Component({
  selector: 'app-price-filter',
  templateUrl: './price-filter.component.html',
  styleUrls: ['./price-filter.component.scss']
})
export class PriceFilterComponent implements OnInit {
  
  constructor(private fb:FormBuilder) { }

  formC:FormGroup

  ngOnInit(): void {
    this.formC=this.fb.group({
      maxPrice:[0,[Validators.required]],
      minPrice:[0,[Validators.required]],
    })
  }

  @Output() setPrixEvent = new EventEmitter<object>();

  clickSubmit(){
        if(this.formC.value.maxPrice > this.formC.value.minPrice){
          this.setPrixEvent.emit({maxPrice: this.formC.value.maxPrice, minPrice: this.formC.value.minPrice})
          return false
        }else if(this.formC.value.maxPrice < this.formC.value.minPrice){
          this.setPrixEvent.emit({maxPrice: this.formC.value.minPrice, minPrice: this.formC.value.maxPrice})
          return false
        }else{
          this.setPrixEvent.emit({maxPrice: this.formC.value.minPrice, minPrice: this.formC.value.maxPrice})
          return false
        }
  }
  

}
