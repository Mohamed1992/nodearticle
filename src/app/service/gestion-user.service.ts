import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import {Http, RequestOptions,Headers} from '@angular/http'
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class GestionUserService {
  
  baseURL = "http://51.75.194.53:3000"
  //baseURL = "http://localhost:3000"


  constructor(private http:Http) {
    this.loggIn()
    this.getRole()
  }
 

  varLoading=1
  loading=new BehaviorSubject(this.varLoading)
  loadingChange=this.loading.asObservable()

  setLoading(){
    this.loading.next(1)
  }

  roleCompte = "client"
  idUser=""
  var=false
  log=new BehaviorSubject(this.var)
  logchange=this.log.asObservable()  

  varRole="client"
  role=new BehaviorSubject(this.varRole)
  roleChange=this.role.asObservable()  

  getRole(){
    this.varRole = localStorage.getItem("Role")
    this.role.next(this.varRole) 
  }

  setRole(role){
    this.varRole = role
    this.role.next(this.varRole) 
  }


  optionHeader() : RequestOptions{
    let token= localStorage.getItem('Token');
    let headers=new Headers();
    headers.append('Authorization', `Bearer ${token}`);
    let options=new RequestOptions({ headers :headers});
    return options
  }
  

  validationCompte(idUser){
    return this.http.get(this.baseURL+"/User/validerCompte/"+idUser).pipe(map(res=>{
      return res.json()
    }))
  }

  envoyerEmailValidation(request){
    return this.http.post(this.baseURL+"/contact/emailValidation", request).pipe(map(res=>{
      return res.json()
    }))
  }

  listeBoutiques(request){
    return this.http.post(this.baseURL+"/User/listesBoutiques", request, this.optionHeader() ).pipe(map(res=>{
      return res.json()
    }))
  }

  listeClients(request){
    return this.http.post(this.baseURL+"/User/listesClients", request, this.optionHeader() ).pipe(map(res=>{
      return res.json()
    }))
  }


  loggIn(){
    if(localStorage.getItem("Token") == undefined){
      this.log.next(false)
      return false 
    }

    if(localStorage.getItem("Token") == null){
      this.log.next(false)
      return false 
    }

   if (localStorage.getItem("Token").length > 10){
    this.log.next(true) 
    return true
   }else{
     this.log.next(false)
     return false
   }
  }

  logout(){
    this.log.next(false)
    localStorage.setItem("Token","")
    localStorage.setItem("Role","")
    this.setRole("")
  }
  
  login(formData){
    return this.http.post(this.baseURL+"/user/login",formData).pipe(map(res=>{
      return res.json()
    }))
  }

  register(formData){
    
    return this.http.post(this.baseURL+"/user/register",formData).pipe(map(res=>{
      return res.json()
    }))
  }

  userById(){
    return this.http.get(this.baseURL+"/user/"+this.idUser,this.optionHeader()).pipe(map(res=>{
      return res.json()
    }))
  }

  activeOrDesactive(isEnabled){
    return this.http.get(this.baseURL+"/user/active/"+this.idUser+"/"+isEnabled,this.optionHeader()).pipe(map(res=>{
      return res.json()
    }))
  }

  monCompte(){
    return this.http.get(this.baseURL+"/user/details/_id", this.optionHeader()).pipe(map(res=>{
      return res.json()
    }))
  }

  UpdateCompte(request){
    return this.http.post(this.baseURL+"/user/update/_id", request, this.optionHeader()).pipe(map(res=>{
      return res.json()
    }))
  }

  envoyerEmail(request){
    return this.http.post(this.baseURL+"/contact/email", request).pipe(map(res=>{
      return res.json()
    }))
  }

}
