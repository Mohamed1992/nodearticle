
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { GestionUserService } from '../service/gestion-user.service';
import {Http, RequestOptions,Headers} from '@angular/http'
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GestionCategorieService {
  


  idCategorie
baseURL = "http://51.75.194.53:3000"
//baseURL = "http://localhost:3000"


  varCategorieFiltration = {categorie:"0", sousCategorie:"0", sousSousCategorie:"0"}
  categorieFiltration=new BehaviorSubject(this.varCategorieFiltration)
  categorieFiltrationChange=this.categorieFiltration.asObservable()

  setCategorieFiltration(categorieFiltration){
    this.varCategorieFiltration = categorieFiltration  
    this.categorieFiltration.next(categorieFiltration)
    localStorage.setItem("CATEGORIEFILTRATION", JSON.stringify(this.varCategorieFiltration));
  }

  getCategorieFiltration(){
    var cate = localStorage.getItem("CATEGORIEFILTRATION")
    if(cate != "undefined"){
      this.varCategorieFiltration = JSON.parse(localStorage.getItem("CATEGORIEFILTRATION"))
      this.categorieFiltration.next(this.varCategorieFiltration)
    }
    return this.varCategorieFiltration
  }

  varLoading=1
  loading=new BehaviorSubject(this.varLoading)
  loadingChange=this.loading.asObservable()
  
  setLoading(){
    this.loading.next(1)
  }

  tabCategories = []
  log=new BehaviorSubject(this.tabCategories)
  logchange=this.log.asObservable() 

  setCategories(categories){
    this.tabCategories = categories
    this.log.next(categories)
  }

  varCategorie = ""
  categorie=new BehaviorSubject(this.varCategorie)
  categoriechange=this.categorie.asObservable() 

  setCategorie(categorie){
    this.varCategorie = categorie
    this.categorie.next(categorie)
    localStorage.setItem("CATEGORIE", JSON.stringify(this.varCategorie));
  }

  getCategorie(){
    var cate = localStorage.getItem("CATEGORIE")
    
    if(cate != "undefined"){
      this.varCategorie = JSON.parse(localStorage.getItem("CATEGORIE"))
      this.categorie.next(this.varCategorie)
    }
    
    return this.varCategorie
  }

  optionHeader() : RequestOptions{
    let token= localStorage.getItem('Token');
    let headers=new Headers();
    headers.append('Authorization', `Bearer ${token}`);
    let options=new RequestOptions({ headers :headers});
    return options
  }

  constructor(private http:Http, private ser:GestionUserService ) {
    this.getCategorie()
    this.getCategorieFiltration()
  }
 

  newCategorie(formData){
    return this.http.post(this.baseURL+"/categorie/newCategorie", formData, this.optionHeader() ).pipe(map(res=>{
      return res.json()
    }))
  }

  listCategories(){
    return this.http.get(this.baseURL+"/categorie/listCategories").pipe(map(res=>{
      return res.json()
    }))
  }
  
  updateCategorie(id, formData){
    return this.http.post(this.baseURL+"/categorie/updateCategorie/"+id, formData, this.optionHeader() ).pipe(map(res=>{
      return res.json()
    }))
  }

  deleteCategorie(id){
    return this.http.get(this.baseURL+"/categorie/deleteCategorie/"+id, this.optionHeader() ).pipe(map(res=>{
      return res.json()
    }))
  }

}