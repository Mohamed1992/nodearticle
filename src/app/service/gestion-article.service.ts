import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';
import { GestionUserService } from '../service/gestion-user.service';
import {Http, RequestOptions,Headers} from '@angular/http';
import { Command } from 'protractor';


@Injectable({
  providedIn: 'root'
})
export class GestionArticleService {
  
  baseURL = "http://51.75.194.53:3000"
  //baseURL = "http://localhost:3000"


  varArticles=[]
  articles=new BehaviorSubject(this.varArticles)
  articlesChange=this.articles.asObservable()
  
  
  varPanier=[]
  panier=new BehaviorSubject(this.varPanier)
  panierChange=this.panier.asObservable()  

  varSearch=""
  search=new BehaviorSubject(this.varSearch)
  searchChange=this.search.asObservable()
  
  varSearch2=""
  search2=new BehaviorSubject(this.varSearch2)
  searchChange2=this.search2.asObservable()
  
  idArticle = ''
  tabArticles=[]
  logged= false;
  
  
  idCommande = ''

  varTabCommandes =[]
  tabCommandes=new BehaviorSubject(this.varTabCommandes)
  tabCommandesChange=this.tabCommandes.asObservable()
  
  setCommandes(commandes){
    this.varTabCommandes = commandes
    this.tabCommandes.next(commandes)
  }

  varLoading=1
  loading=new BehaviorSubject(this.varLoading)
  loadingChange=this.loading.asObservable()

  varLoadingCommandes=1
  loadingCommandes=new BehaviorSubject(this.varLoadingCommandes)
  loadingCommandesChange=this.loadingCommandes.asObservable()

  setLoadingCommandes(){
    this.loadingCommandes.next(1)
  }
  
  setLoading(){
    this.loading.next(1)
  }
  
  optionHeader() : RequestOptions{
    let token= localStorage.getItem('Token');
    let headers=new Headers();
    headers.append('Authorization', `Bearer ${token}`);
    let options=new RequestOptions({ headers :headers});
    return options
  }

  
  constructor(private http:Http, private ser:GestionUserService ) {
    
    this.ser.logchange.subscribe(res=>{
      this.logged=res
    })

    const originalPanier = JSON.parse(localStorage.getItem("PANIER"))
    
    if(originalPanier != null){
      this.varPanier = originalPanier
      this.panier.next(this.varPanier)
    }

  }

  modifierIsShowCommande(id){
    return this.http.get(this.baseURL+"/commande/showAdmin/"+id, this.optionHeader()).pipe(map(res=>{
       return res.json()
    }))
  }
 
  

  createImg(formData){
    return this.http.post(this.baseURL+"/article/upload",formData, this.optionHeader()).pipe(map(res=>{
       return res.json()
    }))
  }

 

  newArticle(formData){
    return this.http.post(this.baseURL+"/article/newArticle", formData, this.optionHeader() ).pipe(map(res=>{
      return res.json()
    }))
  }

  /*listArticle(){
    return this.http.get("http://localhost:3000/article/listArticle").pipe(map(res=>{
      return res.json()
    }))
  }
*/
  articleByIdAdmin(){
      return this.http.get(this.baseURL+"/article/admin/"+this.idArticle,this.optionHeader()).pipe(map(res=>{
        return res.json()
      }))
  }
  
  articleById(){
    return this.http.get(this.baseURL+"/article/articles/"+this.idArticle,this.optionHeader()).pipe(map(res=>{
      return res.json()
    }))
  }
  
  articleById2(){
      var articles = this.varArticles
      var article = articles.filter(x => x.id == this.idArticle)
      if(article.length > 0){
        return article[0]
      }else{
        return {}
      }
  }
  

  

  listArticlesByCategories(request){
      return this.http.post(this.baseURL+"/article/listArticleByCategorie", request).pipe(map(res=>{
        return res.json()
      }))
  }

 
  listArticlesByCategoriesDashboard(request){
      if(this.varSearch2 != ""){
        return this.http.post(this.baseURL+"/article/listArticleByCategorieSearch/"+this.varSearch2, request, this.optionHeader()).pipe(map(res=>{
          return res.json()
        }))
      }else{
        return this.http.post(this.baseURL+"/article/listArticleByCategorieDashboard", request, this.optionHeader()).pipe(map(res=>{
          return res.json()
        }))
      }
  }

  ajouterAuPanier(articleWithQuantite){
    var ok = false
    for(var i =0; i<this.varPanier.length;i++){
      if(articleWithQuantite.article.id == this.varPanier[i].article.id){
        this.varPanier[i] = articleWithQuantite
        ok = true
      }
    }

    if(!ok && articleWithQuantite.quantite > 0){
      this.varPanier.push(articleWithQuantite)
    }else{
      if(articleWithQuantite.quantite == 0){
        const index: number = this.varPanier.indexOf(articleWithQuantite);
        this.varPanier.splice(index, 1);
       }
    }

    this.panier.next(this.varPanier)

    localStorage.setItem("PANIER", JSON.stringify(this.varPanier));

  }

  verifierAuPanier(article){
    var varPanierCopie = this.varPanier
   
    for(var i =0; i<varPanierCopie.length;i++){
      if(varPanierCopie[i].article != undefined){
        if(article.id == varPanierCopie[i].article.id){
          return varPanierCopie[i].quantite
        }
      }else{
        this.varPanier.splice(i, 1);
        this.panier.next(this.varPanier)
        localStorage.setItem("PANIER", JSON.stringify(this.varPanier));
      }
    }

    return '0'
  }

  deleteAuPanier(id){
    var idArticle = -1
    for(var i =0; i<this.varPanier.length;i++){
      if(id == this.varPanier[i].article.id){
        idArticle = i
      }
    }
    
    this.varPanier.splice(idArticle, 1);
    this.panier.next(this.varPanier)
    localStorage.setItem("PANIER", JSON.stringify(this.varPanier));
  }

  viderPanier(){
    this.varPanier = []
  
    this.panier.next(this.varPanier)
  
    localStorage.setItem("PANIER", JSON.stringify(this.varPanier));
  }

  envoyerCommande(){
    var request = []
    
    for(var i =0; i<this.varPanier.length;i++){
      request.push({quantite:""+this.varPanier[i].quantite,
      ref:this.varPanier[i].article.ref,
      image:this.varPanier[i].article.image,
      prix:this.varPanier[i].article.prix,
      newPrix:this.varPanier[i].article.newPrix,
      isPromo:this.varPanier[i].article.isPromo,
      nom:this.varPanier[i].article.nom,
      proprietaire:this.varPanier[i].article.proprietaire,
    })
    }

    return this.http.post(this.baseURL+"/commande/newCommande", request, this.optionHeader() ).pipe(map(res=>{
      return res.json()
    }))
  }

  listCommandes(etat,request){
    return this.http.post(this.baseURL+"/commande/listCommandes/"+etat, request, this.optionHeader()).pipe(map(res=>{
      return res.json()
    }))
  }

  modifierCommande(etat){
    return this.http.get(this.baseURL+"/commande/modifierEtat/"+this.idCommande +"/"+etat, this.optionHeader()).pipe(map(res=>{
      return res.json()
    }))
  }

  activeOrDesactive(isEnabled){
    return this.http.get(this.baseURL+"/article/active/"+this.idArticle+"/"+isEnabled,this.optionHeader()).pipe(map(res=>{
      return res.json()
    }))
  }

  articlesBySearch(search,page){
    let request = {marque:"0", categorie: "0", sousCategorie:"0", sousSousCategorie: "0", page: page}
    return this.http.post(this.baseURL+"/article/cherche/"+search, request).pipe(map(res=>{
      return res.json()
    }))
  }

  setSearch(search){
    this.varSearch = search
    this.search.next(this.varSearch)
  }

  setArticle(article){
    
    let article2 = this.varArticles.filter(x => x.id == article.id)[0]
    
    let index = this.varArticles.indexOf(article2);
    
    if(article2.isEnabled == "1"){
      article2.isEnabled = "0"
    }else{
      article2.isEnabled = "1"
    }
   
    this.varArticles[index] = article2;
    this.articles.next(this.varArticles)
  }

  setArticle2(article, isPromo){
    let article2 = this.varArticles.filter(x => x.id == this.idArticle)[0]
    
    let index = this.varArticles.indexOf(article2);
   
    article2.nom = article.nom
    article2.prix = article.prix
    article2.qtn = article.qtn
    article2.ref = article.ref
    article2.description = article.description
    article2.marque = article.marque
    article2.newPrix = article.newPrix
    article2.pourcentageRemise = article.pourcentageRemise
    article2.dateDebut = article.dateDebut
    article2.dateFin = article.dateFin
    
    if(isPromo){
      article2.isPromo = "1"
    }else{
      article2.isPromo = "0"
    }
    
    this.varArticles[index] = article2;
    this.articles.next(this.varArticles)
  }



  setArticles(articles){
    this.varArticles = articles;
    this.articles.next(this.varArticles)
  }

  setSearch2(search){
    this.varSearch2 = search
    this.search2.next(this.varSearch2)
  }

 
  modifiedArticle(formData){
    return this.http.post(this.baseURL+"/article/modifierArticle/"+this.idArticle, formData, this.optionHeader() ).pipe(map(res=>{
      return res.json()
    }))
  }

  accueil(){
    return this.http.get(this.baseURL+"/article/accueil/_di").pipe(map(res=>{
      return res.json()
    }))
  }


  deleteArticle(id){
    return this.http.get(this.baseURL+"/article/deleteArticle/"+id, this.optionHeader() ).pipe(map(res=>{
      return res.json()
    }))
  }
  
}
