import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { GestionUserService } from '../service/gestion-user.service';
import {Http, RequestOptions,Headers} from '@angular/http'
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EventService {

  optionHeader() : RequestOptions{
    let token= localStorage.getItem('Token');
    let headers=new Headers();
    headers.append('Authorization', `Bearer ${token}`);
    let options=new RequestOptions({ headers :headers});
    return options
  }

  baseURL = "http://51.75.194.53:3000"
  //baseURL = "http://localhost:3000"

  constructor(private http:Http, private ser:GestionUserService ) {
  }
 

   varEvents=[]
   events=new BehaviorSubject(this.varEvents)
   eventsChange=this.events.asObservable()

   eventId 

   getEvent(){
     var event = this.varEvents.filter(x=> x.id == this.eventId)
     return event[0]
   }

   setEvents(events){
    this.varEvents = events;
    this.events.next(this.varEvents)
   }

   createImg(formData){
    return this.http.post(this.baseURL+"/event/upload",formData, this.optionHeader()).pipe(map(res=>{
       return res.json()
    }))
  }

 

  newEvent(formData){
    return this.http.post(this.baseURL+"/event/newEvent", formData, this.optionHeader() ).pipe(map(res=>{
      return res.json()
    }))
  }

  listEvent(){
    return this.http.get(this.baseURL+"/event/events").pipe(map(res=>{
      return res.json()
    }))
  }

  supprimer(){
    this.supprimerEvent()
    return this.http.get(this.baseURL+"/event/supprimerEvent/"+this.eventId, this.optionHeader()).pipe(map(res=>{
      return res.json()
    }))
  }

  supprimerEvent(){
    var idEvent = -1
    for(var i =0; i<this.varEvents.length;i++){
      if(this.eventId == this.varEvents[i].id){
        idEvent = i
      }
    }
    
    this.varEvents.splice(idEvent, 1);
    this.events.next(this.varEvents)
    
  }

  
 
}
