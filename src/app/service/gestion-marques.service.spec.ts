import { TestBed } from '@angular/core/testing';

import { GestionMarquesService } from './gestion-marques.service';

describe('GestionMarquesService', () => {
  let service: GestionMarquesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GestionMarquesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
