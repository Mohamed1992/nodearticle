import { Injectable } from '@angular/core';
import * as io from 'socket.io-client';
import { Observable } from 'rxjs';
import {Http, RequestOptions,Headers} from '@angular/http';
import { map } from 'rxjs/operators';
import { BehaviorSubject } from 'rxjs';
import { GestionUserService } from './gestion-user.service';

@Injectable({
  providedIn: 'root'
})
export class WebSocketService {
  socket: any;

  baseURL = "http://51.75.194.53:3000"
  //baseURL = "http://localhost:3000"

  

  optionHeader() : RequestOptions{
    let token= localStorage.getItem('Token');
    let headers=new Headers();
    headers.append('Authorization', `Bearer ${token}`);
    let options=new RequestOptions({ headers :headers});
    return options
  }

  

  constructor(private http:Http, private serv2:GestionUserService ) {
    this.socket = io(this.baseURL)

    this.listen().subscribe(res=>{
      var messageSocket:any = res
      if(localStorage.getItem("Role") == "admin" && messageSocket.recepteur == "admin"){
         this.varNumberMessages += 1 
         this.setNumberMessage(this.varNumberMessages) 
      }
    })

  }


  connectAdmin(){
    this.getNumberCommandesMessages().subscribe(res =>{
      if(res.status){
        if(res.nbrMessages[0]){
          this.setNumberMessage(res.nbrMessages[0].nomberMessageNonVueAdmin)
          this.setNumberCommandes(res.nbrCommandes)
        }
      }
    })
  }

  connectUser(){
    if(this.getUserRegister() != null){
      this.getNomberMessages(this.getUserRegister().id).subscribe(res =>{
        if(res.status){
          if(res.resultat[0]){
            this.setNumberMessage(res.resultat[0].nomberMessageNonVue)
          }
        }
      })
    }
  }

  getNumberCommandesMessages(){
    return this.http.get(this.baseURL+"/commande/newCommandesMessages", this.optionHeader()).pipe(map(res=>{
      return res.json()
    }))
  }

  
  getNumberMessagesOfAdmin(){
    var totale = 0
    for(let i =0; i < this.varUsers.length; i++){
      totale += this.varUsers[i].nomberMessageNonVueAdmin
    }
    return totale
  }

  setNumberMessage(number){
    this.varNumberMessages = number
    this.numberMessages.next(this.varNumberMessages)
  }

  varNumberMessages=0
  numberMessages=new BehaviorSubject(this.varNumberMessages)
  numberMessagesChange=this.numberMessages.asObservable()

  varNumberCommandes=0
  numberCommandes=new BehaviorSubject(this.varNumberCommandes)
  numberCommandesChange=this.numberCommandes.asObservable()

  setNumberCommandes(number){
    this.varNumberCommandes = number
    this.numberCommandes.next(this.varNumberCommandes)
  }

  setUsers(users){
    this.varUsers = users
    this.users.next(this.varUsers)
  }

  varUsers=[]
  users=new BehaviorSubject(this.varUsers)
  usersChange=this.users.asObservable()
  
  getNomberMessages(id){
    return this.http.get(this.baseURL+"/chat/numberMessages/"+id).pipe(map(res=>{
      return res.json()
    }))
  }

  setNewRegister(data){
    localStorage.setItem("NewRegister201", JSON.stringify(data));
  }

  getUserRegister(){
    return JSON.parse(localStorage.getItem("NewRegister201"))
  }

  verifieRegister(){
    const user = this.getUserRegister()
    if(user == null){
      return false
    }else{
      return true
    }
  }


  renisualiserMessagesAdmin(idUser){
    return this.http.get(this.baseURL+"/chat/renisialiserNumberAdmin/"+idUser, this.optionHeader()).pipe(map(res=>{
      return res.json()
    }))
  }

  renisualiserMessagesUser(idUser){
    return this.http.get(this.baseURL+"/chat/renisialiserNumberUser/"+idUser).pipe(map(res=>{
      return res.json()
    }))
  }

  getUsersAdmin(){
    return this.http.get(this.baseURL+"/chat/users", this.optionHeader()).pipe(map(res=>{
      return res.json()
    }))
  }

  getMessagesAdmin(idUser){
    return this.http.get(this.baseURL+"/chat/userMessages/"+idUser).pipe(map(res=>{
      return res.json()
    }))
  }

  

  register(formData){
    return this.http.post(this.baseURL+"/chat/register", formData).pipe(map(res=>{
      return res.json()
    }))
  }

  newMessage(formData){
    const user = this.getUserRegister()
    const request = {message:formData.message, idUser:user.id, name: user.name, email:user.email }
    return this.http.post(this.baseURL+"/chat/newMessage", request).pipe(map(res=>{
      return res.json()
    }))
  }

  newMessageAdmin(formData,user){
    const request = {message:formData.message, idUser:user.id, name: user.name, email:user.email }
    return this.http.post(this.baseURL+"/chat/newMessageAdmin", request, this.optionHeader()).pipe(map(res=>{
      return res.json()
    }))
  }

  listen(){
    return new Observable((subscriber)=>{
      this.socket.on("chatErrahmaStore", (data) => {
         subscriber.next(data);
      })
    })
  }

  emit(data: any){
    this.socket.emit("chatErrahmaStore", data);
  }

  listenNewRegister(){
    return new Observable((subscriber)=>{
      this.socket.on("newRegister", (data) => {
         subscriber.next(data);
      })
    })
  }

  emitNewRegister(data: any){
    this.socket.emit("newRegister", data);
  }

  listenNewVue(){
    return new Observable((subscriber)=>{
      this.socket.on("newVueMessage", (data) => {
         subscriber.next(data);
      })
    })
  }

  emitNewVue(data: any){
    this.socket.emit("newVueMessage", data);
  }

  

 

}


/*
 listen(eventName: string){
    return new Observable((subscriber)=>{
      this.socket.on(eventName, (data) => {
        subscriber.next(data);
      })
    })
  }

  emit(eventName: string, data: any){
    this.socket.emit(eventName, data);
  }
*/