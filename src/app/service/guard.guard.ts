import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { GestionUserService } from './gestion-user.service';

@Injectable({
  providedIn: 'root'
})
export class GuardGuard implements CanActivate {
  retour
  constructor(private serv:GestionUserService,private router:Router){}
  
  canActivate(next: ActivatedRouteSnapshot,state: RouterStateSnapshot):boolean{
     
     if(this.serv.loggIn()==true){
      this.retour=true
     }
     else
     { 
      this.retour=false
      this.router.navigate(['/'])
     }
    
    return this.retour
  }
  
}
