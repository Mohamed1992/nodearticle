import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { GestionUserService } from '../service/gestion-user.service';
import {Http, RequestOptions,Headers} from '@angular/http'
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GestionMarquesService {

  baseURL = "http://51.75.194.53:3000"
  //baseURL = "http://localhost:3000"

  optionHeader() : RequestOptions{
    let token= localStorage.getItem('Token');
    let headers=new Headers();
    headers.append('Authorization', `Bearer ${token}`);
    let options=new RequestOptions({ headers :headers});
    return options
  }

  idMarque

  varLoading=1
  loading=new BehaviorSubject(this.varLoading)
  loadingChange=this.loading.asObservable()
  
  tabMarques = []
  
  constructor(private http:Http, private ser:GestionUserService ) {
  }
 
  newMarque(formData){
    return this.http.post(this.baseURL+"/marque/newMarque", formData, this.optionHeader() ).pipe(map(res=>{
      return res.json()
    }))
  }

  updateMarque(id, formData){
    return this.http.post(this.baseURL+"/marque/updateMarque/"+id, formData, this.optionHeader() ).pipe(map(res=>{
      return res.json()
    }))
  }

  deleteMarque(id){
    return this.http.get(this.baseURL+"/marque/deleteMarque/"+id, this.optionHeader() ).pipe(map(res=>{
      return res.json()
    }))
  }

  listMarques(){
    return this.http.get(this.baseURL+"/marque/listMarque").pipe(map(res=>{
      return res.json()
    }))
  }

  setLoading(){
    this.loading.next(1)
  }
  
}