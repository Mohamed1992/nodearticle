import { TestBed } from '@angular/core/testing';

import { GestionCategorieService } from './gestion-categorie.service';

describe('GestionCategorieService', () => {
  let service: GestionCategorieService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GestionCategorieService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
