import { Component, OnInit } from '@angular/core';
import { GestionCategorieService } from '../service/gestion-categorie.service';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-delete-categorie',
  templateUrl: './delete-categorie.component.html',
  styleUrls: ['./delete-categorie.component.scss']
})

export class DeleteCategorieComponent implements OnInit {
  
  constructor(private serv:GestionCategorieService, public activeModal: NgbActiveModal) { }

  categorie

  ngOnInit(): void {
    var categories = this.serv.tabCategories.filter(x => x.id == this.serv.idCategorie)
    
    if(categories.length > 0){
      this.categorie = categories[0]
    }
  }

  supprimer(){
    this.serv.deleteCategorie(this.categorie.id).subscribe(res =>{
      if(res.status){
        alert("categorie est supprimer")
        this.activeModal.close()
        this.serv.setLoading()
      }
    })
  }

}