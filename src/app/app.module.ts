import { BrowserModule } from '@angular/platform-browser';

import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { AddArticleComponent } from './add-article/add-article.component';
import { ListArticleComponent } from './list-article/list-article.component';
import { HttpModule } from "@angular/http"
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { Navbar1Component } from './navbar1/navbar1.component';
import { NavbarSearchComponent } from './navbar-search/navbar-search.component';
import { ArticleDetailComponent } from './article-detail/article-detail.component';
import { RegisterComponent } from './register/register.component';
import { ContactComponent } from './contact/contact.component';
import { AccueilComponent } from './accueil/accueil.component';
import { PresentationComponent } from './presentation/presentation.component';
import { ArticlesComponent } from './articles/articles.component';

import { NavbarSiteComponent } from './navbar-site/navbar-site.component';
import { BoutiquesComponent } from './boutiques/boutiques.component';
import { ClientsComponent } from './clients/clients.component';
import { CommandesComponent } from './commandes/commandes.component';
import { ProfilComponent } from './profil/profil.component';
import { CategoriesComponent } from './categories/categories.component';
import { DetailsUserComponent } from './details-user/details-user.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AddCategorieComponent } from './add-categorie/add-categorie.component';
import { PageArticlesComponent } from './page-articles/page-articles.component';
import { ArticleComponent } from './article/article.component';
import { NavbarPanierComponent } from './navbar-panier/navbar-panier.component';
import { PagePanierComponent } from './page-panier/page-panier.component';
import { CommandeDetailsComponent } from './commande-details/commande-details.component';
import { PageRechercheComponent } from './page-recherche/page-recherche.component';
import { PaginationComponent } from './pagination/pagination.component';
import { ArticleUpdateComponent } from './article-update/article-update.component';
import { ParametresComponent } from './parametres/parametres.component';
import { ItemNavbarPanierComponent } from './item-navbar-panier/item-navbar-panier.component';
import { ItemPagePanierComponent } from './item-page-panier/item-page-panier.component';
import { EventComponent } from './event/event.component';
import { EventsComponentComponent } from './events-component/events-component.component';
import { DeleteEventComponent } from './delete-event/delete-event.component';
import { PagesArticlesComponent } from './pages-articles/pages-articles.component';
import { MarquesComponent } from './marques/marques.component';
import { AddMarqueComponent } from './add-marque/add-marque.component';
import { ModefierMarqueComponent } from './modefier-marque/modefier-marque.component';
import { DeleteMarqueComponent } from './delete-marque/delete-marque.component';
import { UpdateCategorieComponent } from './update-categorie/update-categorie.component';
import { DeleteCategorieComponent } from './delete-categorie/delete-categorie.component';
import { DeleteArticleComponent } from './delete-article/delete-article.component';
import { FooterComponent } from './footer/footer.component';
import { LibrairieComponent } from './librairie/librairie.component';
import { InformatiqueComponent } from './informatique/informatique.component';
import { TissuComponent } from './tissu/tissu.component';
import { BoutiqueComponent } from './boutique/boutique.component';
import { MaisonComponent } from './maison/maison.component';
import { ElectromenageComponent } from './electromenage/electromenage.component';
import { CosmetiqueComponent } from './cosmetique/cosmetique.component';
import { TelephoneComponent } from './telephone/telephone.component';
import { JouetComponent } from './jouet/jouet.component';
import { IndexBarComponent } from './index-bar/index-bar.component';
import { CadeauxComponent } from './cadeaux/cadeaux.component';
import { ValideEmailComponent } from './valide-email/valide-email.component';
import { LoadingComponent } from './loading/loading.component';
import { ChatComponent } from './chat/chat.component';
import { MessageChatComponent } from './message-chat/message-chat.component';
import { MessagesPageComponent } from './messages-page/messages-page.component';
import { PriceFilterComponent } from './price-filter/price-filter.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    AddArticleComponent,
    ListArticleComponent,
    Navbar1Component,
    NavbarSearchComponent,
    ArticleDetailComponent,
    RegisterComponent,
    ContactComponent,
    AccueilComponent,
    PresentationComponent,
    ArticlesComponent,
    NavbarSiteComponent,
    BoutiquesComponent,
    ClientsComponent,
    CommandesComponent,
    ProfilComponent,
    CategoriesComponent,
    DetailsUserComponent,
    AddCategorieComponent,
    PageArticlesComponent,
    ArticleComponent,
    NavbarPanierComponent,
    PagePanierComponent,
    CommandeDetailsComponent,
    PageRechercheComponent,
    PaginationComponent,
    ArticleUpdateComponent,
    ParametresComponent,
    ItemNavbarPanierComponent,
    ItemPagePanierComponent,
    EventComponent,
    EventsComponentComponent,
    DeleteEventComponent,
    PagesArticlesComponent,
    MarquesComponent,
    AddMarqueComponent,
    ModefierMarqueComponent,
    DeleteMarqueComponent,
    UpdateCategorieComponent,
    DeleteCategorieComponent,
    DeleteArticleComponent,
    FooterComponent,
    LibrairieComponent,
    InformatiqueComponent,
    TissuComponent,
    BoutiqueComponent,
    MaisonComponent,
    ElectromenageComponent,
    CosmetiqueComponent,
    TelephoneComponent,
    JouetComponent,
    IndexBarComponent,
    CadeauxComponent,
    ValideEmailComponent,
    LoadingComponent,
    ChatComponent,
    MessageChatComponent,
    MessagesPageComponent,
    PriceFilterComponent,
   
  ],
  imports: [HttpModule, Ng2SearchPipeModule,
    BrowserModule,
    AppRoutingModule,
    FormsModule, ReactiveFormsModule, NgbModule, BrowserAnimationsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
