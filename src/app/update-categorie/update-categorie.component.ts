import { Component, OnInit } from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { GestionCategorieService } from '../service/gestion-categorie.service';
import { GestionArticleService } from '../service/gestion-article.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-update-categorie',
  templateUrl: './update-categorie.component.html',
  styleUrls: ['./update-categorie.component.scss']
})

export class UpdateCategorieComponent implements OnInit {
  formC:FormGroup
  
  
  categorie

  
  

  constructor(private fb:FormBuilder, public activeModal: NgbActiveModal, private serv:GestionCategorieService, private serv2:GestionArticleService) {
    
     
      
    var categories = this.serv.tabCategories.filter(x => x.id == this.serv.idCategorie)
    
    if(categories.length > 0){
      this.categorie = categories[0]
    }
    
    this.formC=this.fb.group({
      nom:[this.categorie.nom,[Validators.required]],
    })

    //$('#00').val(this.marque.categorie.id)
  }

  ngOnInit(): void {
  }

  
  updateCategorie(){

    if(this.formC.value.nom == ""){
      alert("SVP inserer le nom")
    }else{

          var request = {nom:this.formC.value.nom}
             
          this.serv.updateCategorie(this.categorie.id, request).subscribe(res => {
            if(res.status){
              alert("categorie modifier")
              this.activeModal.close()
              this.serv.setLoading()
            }
          }, err =>{
            alert(err._body)
          
          })
    }
    
  }

}

