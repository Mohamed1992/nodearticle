import { Component, OnInit } from '@angular/core';
import { GestionMarquesService } from '../service/gestion-marques.service';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-delete-marque',
  templateUrl: './delete-marque.component.html',
  styleUrls: ['./delete-marque.component.scss']
})
export class DeleteMarqueComponent implements OnInit {

  constructor(private serv:GestionMarquesService, public activeModal: NgbActiveModal) { }

  marque

  ngOnInit(): void {
    var marques = this.serv.tabMarques.filter(x => x.id == this.serv.idMarque)
    
    if(marques.length > 0){
      this.marque = marques[0]
    }
  }

  supprimer(){
    this.serv.deleteMarque(this.marque.id).subscribe(res =>{
      if(res.status){
        alert("marque est supprimer")
        this.activeModal.close()
        this.serv.setLoading()
      }
    })
    
  }

}

