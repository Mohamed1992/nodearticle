import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteMarqueComponent } from './delete-marque.component';

describe('DeleteMarqueComponent', () => {
  let component: DeleteMarqueComponent;
  let fixture: ComponentFixture<DeleteMarqueComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeleteMarqueComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteMarqueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
