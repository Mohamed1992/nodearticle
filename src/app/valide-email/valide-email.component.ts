import { Component, OnInit } from '@angular/core';
import { GestionUserService } from '../service/gestion-user.service';
import { ActivatedRoute, Router } from '@angular/router';
declare var $: any;

@Component({
  selector: 'app-valide-email',
  templateUrl: './valide-email.component.html',
  styleUrls: ['./valide-email.component.scss']
})
export class ValideEmailComponent implements OnInit {
  
  id;
  private sub: any;

  constructor( private route: ActivatedRoute, private route2: Router, private serv:GestionUserService) {
    
  }

  ngOnInit(): void {
    $("html, body").animate({scrollTop : 500},700);
    this.sub = this.route.params.subscribe(params => {
      this.id = params['id']; // (+) converts string 'id' to a number
      console.log(this.id)
      // In a real app: dispatch action to load the details here.
   });
  }

  isLoading = false
  valideCompte(){
    if(!this.isLoading){
      this.isLoading = true
    
      this.serv.validationCompte(this.id).subscribe(res => {
        this.isLoading = false
     
        alert("Félicitations ! Votre compte est activé.")
        $("html, body").animate({scrollTop : 0},700);
        this.route2.navigate(['/accueil'])
    }, err =>{
      this.isLoading = false
    
        alert("erreur")
    
    })
    }
    
  }

}
