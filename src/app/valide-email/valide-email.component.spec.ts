import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ValideEmailComponent } from './valide-email.component';

describe('ValideEmailComponent', () => {
  let component: ValideEmailComponent;
  let fixture: ComponentFixture<ValideEmailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ValideEmailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ValideEmailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
