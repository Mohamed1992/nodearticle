import { Component, OnInit } from '@angular/core';
import { GestionArticleService } from '../service/gestion-article.service';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-delete-article',
  templateUrl: './delete-article.component.html',
  styleUrls: ['./delete-article.component.scss']
})
export class DeleteArticleComponent implements OnInit {

  constructor(private serv:GestionArticleService, public activeModal: NgbActiveModal) { }

  article

  ngOnInit(): void {
    this.serv.articleById().subscribe(res=>{
      this.article=res.resultat
    })
  }

  supprimer(){
    this.serv.deleteArticle(this.article.id).subscribe(res =>{
      if(res.status){
        alert("article est supprimer")
        this.activeModal.close()
        this.serv.setLoading()
      }
    })
  }

}