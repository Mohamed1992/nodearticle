import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { GestionArticleService } from '../service/gestion-article.service';
import { GestionCategorieService } from '../service/gestion-categorie.service';
declare var $: any;
import {NgForm} from '@angular/forms';

@Component({
  selector: 'app-add-article',
  templateUrl: './add-article.component.html',
  styleUrls: ['./add-article.component.scss']
})
export class AddArticleComponent implements OnInit {

  tabArticles
  formC:FormGroup
  selectedFile = null
  
  idCategorie = '1'

  idSousCategorie = '0'

  tabCategoriesGlobal = []

  tabCategories = []

  tabSousCategories = []

  tabMarques = []
  listMarques = []

  isLoading = false
  
  isPromo = false
  
  public imagePath;
  imgURL: any;
  public message: string;

  isAdmin

  marque : String = "Autres"

  constructor(private fb:FormBuilder,private serv:GestionArticleService, private servCat:GestionCategorieService){
    
    this.inisializeFormulaire()
    
    this.servCat.listCategories().subscribe(res=>{
      
      this.servCat.setCategories(res.resultat)

      this.tabCategoriesGlobal = res.resultat
      
      this.tabMarques = res.marques

      this.servCat.tabCategories = this.tabCategoriesGlobal
     
      this.tabCategories = this.tabCategoriesGlobal.filter(x=> x.categorie == "0")  

      for(var i = 0; i<this.tabCategories.length; i++ ){
        var listSousCategories = this.tabCategoriesGlobal.filter(x=> x.categorie == this.tabCategories[i].id)
        
        var sousCategoriesWithSousSous = []
        
        for(var j = 0; j<listSousCategories.length; j++ ){
          sousCategoriesWithSousSous.push({sousCategorie: listSousCategories[j], sousSousCategories: this.tabCategoriesGlobal.filter(x=> x.categorie == listSousCategories[j].id) })
        }

        this.tabSousCategories.push({categorie:this.tabCategories[i], sousCategories:  sousCategoriesWithSousSous })
      }

    })
    
    this.isAdmin = localStorage.getItem("Role") == "admin" 
  }

  controleInput(obj){

    if(obj.nom == ""){
      alert("SVP, inserer le nom") 
      return false
    }else if(obj.prix == 0){
      alert("SVP, inserer le prix") 
      return false
    }else if(obj.categorie == "0"){
      alert("SVP, inserer le categorie") 
      return false
    }else if(obj.marque == ""){
      alert("SVP, inserer la marque") 
      return false
    } 
    return true
  }
  
  

  ngOnInit(){
    this.tabArticles=this.serv.tabArticles
  }

  multiImage
  selectedM(event) {
     this.multiImage = event.target.files;
     var files = event.target.files;
     if (files.length === 0)
     return;

     var mimeType = files[0].type;
     if (mimeType.match(/image\/*/) == null) {
       this.message = "Only images are supported.";
       return;
     }

     var reader = new FileReader();
     this.imagePath = files;
     reader.readAsDataURL(files[0]); 
     reader.onload = (_event) => { 
       this.imgURL = reader.result; 
     }

  }

  ajout(){
    
    if(this.formC.value.categorie == "" && this.formC.value.categorie == undefined){
      this.formC.value.categorie = this.tabCategories[0].id
    }

    if(this.formC.value.sousCategorie == "" || this.formC.value.sousCategorie == "0"){
      this.formC.value.sousCategorie = "0"
    }else{
      let listCats = this.tabCategoriesGlobal.filter(x=> x.id == this.formC.value.sousCategorie)
     console.log(listCats)
      if(listCats[0].categorie != this.formC.value.categorie ){
        this.formC.value.sousCategorie = "0"
      }

    }

    if(this.formC.value.sousSousCategorie == ""){
      this.formC.value.sousSousCategorie = "0"
    }else{
      let listCats = this.tabCategoriesGlobal.filter(x=> x.id == this.formC.value.sousSousCategorie)
      if(listCats.length > 0 ){
      
        if(listCats[0].categorie != this.formC.value.sousCategorie ){
          this.formC.value.sousSousCategorie = "0"
        }
      }else{
        this.formC.value.sousSousCategorie = "0"
      }

    }

    if(!this.isPromo){
        this.formC.value.newPrix = 0
        this.formC.value.pourcentageRemise = 0
        this.formC.value.dateDebut = "00"
        this.formC.value.dateFin = "00"
    }

  /*  if(this.formC.value.marque != "0"){
      var marqueSelected = this.tabMarques.filter(x => x.id == this.formC.value.marque)
      
      if(marqueSelected.length > 0){
        if(marqueSelected[0].categorie != this.formC.value.categorie ){
            this.formC.value.marque = "0"
        }
      }

    }
  */
    this.formC.value.marque = this.marque
    if(!this.controleInput(this.formC.value)){
       return false
    }
    
    if(this.multiImage && !this.isLoading && this.imgURL != ""){
      const formData = new FormData();
      this.isLoading = true
      for (let img of this.multiImage)
        formData.append('myFiles', img)
        
        this.serv.createImg(formData).subscribe(res => {
          
          let obj = this.formC.value
          obj.image = res[0]
          obj.marque = this.marque
          if(this.controleInput(obj)){
            this.serv.newArticle(obj).subscribe(res => {
              if(res.status){
                this.isLoading = false
                alert("article ajouter")

                this.marque = "Autres"
                this.inisializeFormulaire()
                this.imgURL = ""
              }else{
                this.isLoading = false
                alert("Erreur, l'article n'ajouter pas")
              }
            },err =>{
              this.isLoading = false
              alert("erreur d'ajouter article")
            })
          }else{
            this.isLoading = false
          }
        },err =>{
          this.isLoading = false
          alert("erreur de copier image")
        })
    }else{
      this.isLoading = false
      alert("inserer l'image")
    }
   
  }


  inisializeFormulaire(){
    
    this.formC=this.fb.group({
      nom:['',[Validators.required]],
      prix:['',[Validators.required]],
      ref:['',[Validators.required]],
      qtn:['',[Validators.required]],
      description:['',[Validators.required]],
      marque:['Autres',[Validators.required]],
      categorie:['0',[Validators.required]],
      sousCategorie:['0',[Validators.required]],
      sousSousCategorie:['0',[Validators.required]],
      image:['',[Validators.required]],
      newPrix:['',[Validators.required]],
      pourcentageRemise:['',[Validators.required]],
      dateDebut:['',[Validators.required]],
      dateFin:['',[Validators.required]],
    })
  }

  changeCategorie(event){
    this.formC.value.sousCategorie == ''
    
    if(event.target.value !=''){
      this.idCategorie =event.target.value
    }
    
   // this.setMarques(this.idCategorie,"0","0")

    var elements = document.getElementsByClassName("sous-categorie-22")
   // var marques = document.getElementsByClassName("marques-22")
    
    //marques[0].setAttribute("style", "visibility: hidden;")
    //marques[1].setAttribute("style", "")
    
    for (let i = 0; i <elements.length; i++){
      
      if(elements[i].id == this.idCategorie){
         elements[i].setAttribute("style", "  ")
    
      }else{
        elements[i].setAttribute("style", "visibility: hidden;")
      }
    }

    this.initializeSousSousCategorie()
    this.initializeSousCategorie()
//    this.initializeMarques()

    var sousElements = document.getElementsByClassName("sous-categorie-33")
   
  

    for (let i = 0; i <sousElements.length; i++){
      
      if(sousElements[i].id == "1000"){
        sousElements[i].setAttribute("style", "  ")
      }else{
        sousElements[i].setAttribute("style", "visibility: hidden;")
      }
    }

  }

  changeSousCategorie(event){
    if(event.target.value !=''){
      this.idSousCategorie =event.target.value
    }
     
   // this.setMarques("0",this.idSousCategorie,"0")

    var elements = document.getElementsByClassName("sous-categorie-33")
   
  

    for (let i = 0; i <elements.length; i++){
      
      if(elements[i].id == this.idSousCategorie){
         elements[i].setAttribute("style", "  ")
      }else{
        elements[i].setAttribute("style", "visibility: hidden;")
      }
    }

    this.initializeSousSousCategorie()

  }

  initializeSousCategorie(){
    var elements = document.getElementsByClassName("sous-categorie-22")
   
    for (let i = 0; i <elements.length; i++){
      $('#'+elements[i].id).val(0) 
    }

  }

 /* initializeMarques(){
    var elements = document.getElementsByClassName("marques-22")
   
    for (let i = 0; i <elements.length; i++){
      $('#'+elements[i].id).val(0) 
    }

  }
*/
  initializeSousSousCategorie(){
    var elements = document.getElementsByClassName("sous-categorie-33")
   
    for (let i = 0; i <elements.length; i++){
      $('#'+elements[i].id).val(0) 
    }

  }

  changePromotion(){
     this.isPromo = !this.isPromo
  }  

  changeSousSousCategorie(event){
//    this.setMarques("0","0", event.target.value)
  }

  setMarques(categorieId, sousCategorieId, sousSousCategorieId){
    
    var listMarques2 = this.tabMarques
    
 //   if(categorieId != "0"){
      listMarques2 = listMarques2.filter(x => (x.categorie == categorieId && x.sousCategorie == "0" && x.sousSousCategorie == "0"))
      this.filterMarques(listMarques2)  
   /* }else if(sousCategorieId != "0"){
      listMarques2 = listMarques2.filter(x => x.sousCategorie == sousCategorieId)
      this.filterMarques(listMarques2)
    }else if(sousSousCategorieId != "0"){
      listMarques2 = listMarques2.filter(x => x.sousSousCategorie == sousSousCategorieId)
      this.filterMarques(listMarques2)
    }
    */

  }

  marquesFilters = []

  selectMarques(){
    var listMarques2 = this.tabMarques
    
    listMarques2 = listMarques2.filter(x => (x.nom.toLowerCase( ).indexOf(this.marque.toLowerCase( )) == 0))
    this.filterMarques(listMarques2)  
    
  }

  filterMarques(listMarques2){
    var newMarques = []
      
    for(var i =0; i<listMarques2.length;i++){
      var somme = 0
       for(var j = 0; j < newMarques.length;j++){
         if(listMarques2[i].nom == newMarques[j].nom){
            somme++
         }
       }
       if(somme == 0){
          newMarques.push(listMarques2[i])
       }
    }
    
    this.marquesFilters = newMarques
    
  }

  setMarque(event){
    this.marque = event.target.id
  }
}


