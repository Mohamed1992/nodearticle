import { Component, OnInit } from '@angular/core';
import { GestionArticleService } from '../service/gestion-article.service';
import { ActivatedRoute } from '@angular/router';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, Validators,FormControl } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-article-detail',
  templateUrl: './article-detail.component.html',
  styleUrls: ['./article-detail.component.scss']
})
export class ArticleDetailComponent implements OnInit {
  formC:FormGroup
  article
  quantitePanier =1
  isAchete = false
  isAdmin = false
  isEnabled = false
  isDashboard = false
  isPromo = false
  isList = false
  loading = false
  isFacilite = false

  constructor(private fb:FormBuilder, private route:ActivatedRoute,private serv:GestionArticleService, public activeModal: NgbActiveModal, private router : Router) {
    this.formC=this.fb.group({
      quantite:new FormControl(this.quantitePanier, [Validators.required, Validators.pattern('^[1-9]\\d*$')])
    })
  }

  ngOnInit() {
    // this.id=this.route.snapshot.paramMap.get("id")
    this.isAdmin = localStorage.getItem("Role") == "admin"
    
    this.isDashboard = this.router.url == "/articles" || this.router.url == "/parametres" 
    
    this.isList = this.router.url != "/accueil" && this.router.url != "/parametres" && this.router.url != "/recherche" && this.router.url != "/panier"
    
    if(localStorage.getItem("Role") == "admin"){
      this.loading = true
      this.serv.articleByIdAdmin().subscribe(res=>{
        this.loading = false
        this.article=res.resultat
        
        this.isFacilite = this.article.isFacilite == "1"
        this.isPromo = this.article.isPromo == "1"
        this.isEnabled = this.article.isEnabled == "1"
        this.quantitePanier = Number(this.serv.verifierAuPanier(this.article))
        
        if(this.quantitePanier > 0){
          this.isAchete = true
          this.formC=this.fb.group({
            quantite:new FormControl(this.quantitePanier, [Validators.required, Validators.pattern('^[1-9]\\d*$')])
          })
        }
        
      })

    }else{
      if(this.isList){
      this.article=this.serv.articleById2()
      
      this.isFacilite = this.article.isFacilite == "1"
      this.isPromo = this.article.isPromo == "1"
      this.isEnabled = this.article.isEnabled == "1"
      this.quantitePanier = Number(this.serv.verifierAuPanier(this.article))
      
      if(this.quantitePanier > 0){
        this.isAchete = true
        this.formC=this.fb.group({
          quantite:new FormControl(this.quantitePanier, [Validators.required, Validators.pattern('^[1-9]\\d*$')])
        })
      }

    }else{
      this.loading = true
      this.serv.articleById().subscribe(res=>{
        this.loading = false
        this.article=res.resultat
        
        this.isFacilite = this.article.isFacilite == "1"
        this.isPromo = this.article.isPromo == "1"
        this.isEnabled = this.article.isEnabled == "1"
        this.quantitePanier = Number(this.serv.verifierAuPanier(this.article))
        
        if(this.quantitePanier > 0){
          this.isAchete = true
          this.formC=this.fb.group({
            quantite:new FormControl(this.quantitePanier, [Validators.required, Validators.pattern('^[1-9]\\d*$')])
          })
        }
        
      })
    }
  }
    
  }

  ajouterAuPanier(){
   let qtn = this.formC.value.quantite + ""
    var index = qtn.indexOf( "." ); 
    if(index == -1){
      this.serv.ajouterAuPanier({quantite:this.formC.value.quantite,article:this.article})
      this.activeModal.close()
    }else{
      alert("SVP, inserez un entier")
    }
  }

  activeOrDesactive(){
    var isEnabled = this.article.isEnabled == "1" ? "0" : "1"
  
    this.serv.activeOrDesactive(isEnabled).subscribe(res=>{
      this.serv.setArticle(this.article)
      if(this.isEnabled)
        this.article.isEnabled = "0"
      else  
        this.article.isEnabled = "1"
      
      this.isEnabled = !this.isEnabled
    })
  }

}
