
import { Component, OnInit } from '@angular/core';
import { GestionArticleService } from '../service/gestion-article.service';
import { ArticleDetailComponent } from '../article-detail/article-detail.component';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { LoginComponent } from '../login/login.component';


@Component({
  selector: 'app-page-panier',
  templateUrl: './page-panier.component.html',
  styleUrls: ['./page-panier.component.scss']
})
export class PagePanierComponent implements OnInit {
  panier=[]
  length = 0
  prixTotal = 0
  constructor(private serv:GestionArticleService, private modalService: NgbModal) { }
  
  isLoading = false
  
  ngOnInit(): void {
    this.serv.panierChange.subscribe(res=>{
      this.panier = res
      this.length = this.panier.length 
      this.calculerPrixTotal()
    })
  }

  calculerPrixTotal(){
    var totalPrix = 0
    for(var i=0;i<this.panier.length;i++){
     
      var prixUnitaire = 0
      if(this.panier[i].article != undefined){
     
         if(this.panier[i].article.isPromo == "1"){
           prixUnitaire = Number(this.panier[i].article.newPrix)
         }else{
           prixUnitaire = Number(this.panier[i].article.prix)
         }

         const quantite = Number(this.panier[i].quantite)
         totalPrix +=  quantite * prixUnitaire
      }
    }
    this.prixTotal = totalPrix
  }

  openDetailsArticle(event) {
    this.serv.idArticle = event.target.id
    const modalRef = this.modalService.open(ArticleDetailComponent, {size:"lg"});
    modalRef.componentInstance.name = 'World';
  }

  envoyer(){
    var ok = true
    if (localStorage.getItem("Token") == "undefined" || localStorage.getItem("Token") == null || localStorage.getItem("Token").length < 10){
      this.openLogin()
      ok =false
      return false
    }

    if (ok){
      if(localStorage.getItem("Role") == "client" && !this.isLoading){
        this.isLoading = true
          this.serv.envoyerCommande().subscribe(res => {
            if(res.status){
              this.isLoading = false
              alert("Votre commande envoye avec réussir")
              this.serv.viderPanier()
            }else{
              this.isLoading = false
                alert("Désole, votre commande n'envoye pas")
            }
            
          },err =>{
            this.isLoading = false
              alert("erreur")
          })
      }else{
        this.isLoading = false
        alert("Désole, vous n'avez pas l'accée")
      }
    }
  }

  openLogin() {
    const modalRef = this.modalService.open(LoginComponent);
    modalRef.componentInstance.name = 'World';
  }

  viderPanier(){
    this.serv.viderPanier()
  }
}
