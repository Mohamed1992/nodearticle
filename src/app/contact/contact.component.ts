import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { GestionUserService } from '../service/gestion-user.service';


@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {
  formC:FormGroup
 
  constructor(private fb:FormBuilder, private serv:GestionUserService) { }

  ngOnInit(): void {
    this.formC=this.fb.group({
      nom:['',[Validators.required]],
      email:['',[Validators.required]],
      subject:['',[Validators.required]],
      company:['',[Validators.required]],
      message:['',[Validators.required]],
    })
  }
isLoading = false
  envoyerMessage(){
    if(this.controleInput(this.formC.value) && !this.isLoading){
      this.isLoading = true 
      this.serv.envoyerEmail(this.formC.value).subscribe(res => {
      
        this.isLoading = false 
        alert("email envoyer")  
       }, err =>{
        this.isLoading = false
         alert("erreur")
       })
    }
  }

  controleInput(obj){
    if(obj.nom == ""){
      alert("SVP, inserez votre nom") 
      return false
    }else if(obj.email == ""){
      alert("SVP, inserez votre email") 
      return false
    }else if(obj.subject == ""){
      alert("SVP, inserez votre sujet") 
      return false
    }else if(obj.company == ""){
        alert("SVP, inserez le nom de votre société") 
        return false
    }else if(obj.message == ""){
        alert("SVP, inserez votre message") 
        return false
    }
     
    return true
  }
}
