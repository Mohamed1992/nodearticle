import { Component, OnInit } from '@angular/core';
import { EventService } from '../service/event.service';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-delete-event',
  templateUrl: './delete-event.component.html',
  styleUrls: ['./delete-event.component.scss']
})
export class DeleteEventComponent implements OnInit {

  constructor(private serv:EventService, public activeModal: NgbActiveModal) { }

  event

  ngOnInit(): void {
  this.event = this.serv.getEvent()
  }

  supprimer(){
    this.serv.supprimer().subscribe(res =>{
      alert("event est suprimeé")
      this.activeModal.close()  
    },err =>{
      alert("erreur")
    })
  }

}
