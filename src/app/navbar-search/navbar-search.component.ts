import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { GestionArticleService } from '../service/gestion-article.service';

@Component({
  selector: 'app-navbar-search',
  templateUrl: './navbar-search.component.html',
  styleUrls: ['./navbar-search.component.scss']
})
export class NavbarSearchComponent implements OnInit {
  formC:FormGroup
  constructor(private fb:FormBuilder, private router:Router,private serv:GestionArticleService) { 
    this.formC=this.fb.group({
      search:['',[Validators.required]]
    })
  }

  ngOnInit(): void {
  }

  recherche(){
    if(this.formC.value.search != ""){
      this.serv.setSearch(this.formC.value.search)
      this.router.navigate(['/recherche'])
    }
  }
}
