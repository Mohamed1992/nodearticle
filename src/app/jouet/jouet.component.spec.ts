import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JouetComponent } from './jouet.component';

describe('JouetComponent', () => {
  let component: JouetComponent;
  let fixture: ComponentFixture<JouetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JouetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JouetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
