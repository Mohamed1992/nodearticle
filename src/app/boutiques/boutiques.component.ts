import { Component, OnInit } from '@angular/core';
import { GestionUserService } from '../service/gestion-user.service';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DetailsUserComponent } from '../details-user/details-user.component';
import { RegisterComponent } from '../register/register.component';

@Component({
  selector: 'app-boutiques',
  templateUrl: './boutiques.component.html',
  styleUrls: ['./boutiques.component.scss']
})
export class BoutiquesComponent implements OnInit {
  tabBoutiques=[]
  page = 1
  totalPage=1
  
  constructor(private serv:GestionUserService, private modalService: NgbModal) {
    this.serv.loadingChange.subscribe(res =>{
        this.getBoutiques()
      
    })
   }

  ngOnInit(): void {
  }

  getBoutiques(){
    let request = {page:this.page}
    this.serv.listeBoutiques(request).subscribe(res=>{
      console.log(res.resultat.docs)
      this.tabBoutiques = res.resultat.docs
      this.totalPage = res.resultat.pages
      this.page = res.resultat.page
    })
  }

  openDetailsBoutique(event) {
    this.serv.idUser = event.target.id
    const modalRef = this.modalService.open(DetailsUserComponent);
    modalRef.componentInstance.name = 'World';
  }

  openRegister() {
    this.serv.roleCompte = "boutique"
    const modalRef = this.modalService.open(RegisterComponent, {size:"lg"});
    modalRef.componentInstance.name = 'World';
  }

  setPage(newPage: number) {
    this.page = newPage
    this.getBoutiques()
  }

}
