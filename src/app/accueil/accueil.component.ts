import { Component, OnInit } from '@angular/core';
import { GestionArticleService } from '../service/gestion-article.service';
import { GestionCategorieService } from '../service/gestion-categorie.service';
import { ArticleDetailComponent } from '../article-detail/article-detail.component';

import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-accueil',
  templateUrl: './accueil.component.html',
  styleUrls: ['./accueil.component.scss']
})
export class AccueilComponent implements OnInit {
  
  promos=[]
  tabCategoriesGlobal = []
  tabCategories = []
  
  categorieParDefaut
  promosParDefaut
  
  tabPromos = []

  populaires = []

  topCategories = []
  
  constructor(private serv:GestionArticleService, private servCat:GestionCategorieService, private modalService: NgbModal) { 

  }


  ngOnInit(): void {
    this.serv.loadingChange.subscribe(res =>{
      this.getParametres()   
    }) 
  }

 getParametres(){
  this.servCat.listCategories().subscribe(res=>{
      
    this.servCat.setCategories(res.resultat)
    this.tabCategoriesGlobal = res.resultat
    this.servCat.tabCategories = this.tabCategoriesGlobal
  
    this.tabCategories = this.tabCategoriesGlobal.filter(x=> x.categorie == "0")  

    if(this.tabCategories.length){
      this.categorieParDefaut = this.tabCategories[0]
    }
    
    this.serv.accueil().subscribe(res=>{
       if(res.status){
         this.promos = res.promos
         if(this.categorieParDefaut){
           this.promosParDefaut = this.promos.filter(x=> x.categorie == this.categorieParDefaut.id)
         }
         
         this.tabPromos = [] 
         
         for(var i = 1; i<this.tabCategories.length;i++){
           this.tabPromos.push({categorie:this.tabCategories[i], promos: this.promos.filter(x=> x.categorie == this.tabCategories[i].id)})
         }

         this.populaires = res.populaires

         this.topCategories = res.topCategories
       }
     })
  })
 }

  
  openDetailsArticle(event) {
    this.serv.idArticle = event.target.id
    const modalRef = this.modalService.open(ArticleDetailComponent, {size:"lg"});
    modalRef.componentInstance.name = 'World';
  }

}
