
import { Component, OnInit } from '@angular/core';
import { GestionUserService } from '../service/gestion-user.service';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DetailsUserComponent } from '../details-user/details-user.component';

@Component({
  selector: 'app-clients',
  templateUrl: './clients.component.html',
  styleUrls: ['./clients.component.scss']
})
export class ClientsComponent implements OnInit {

  tabClients=[]
page=1
totalPage = 1
  constructor(private serv:GestionUserService, private modalService: NgbModal) { }

  ngOnInit(): void {
   this.getClients()
  }

  getClients(){
    let request = {page:this.page}
    this.serv.listeClients(request).subscribe(res=>{
      this.tabClients = res.resultat.docs
      this.totalPage = res.resultat.pages
      this.page = res.resultat.page
    })
  }

  openDetailsBoutique(event) {
    this.serv.idUser = event.target.id
    const modalRef = this.modalService.open(DetailsUserComponent);
    modalRef.componentInstance.name = 'World';
  }

  setPage(newPage: number) {
    this.page = newPage
    this.getClients()
  }
}
