import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NavbarPanierComponent } from './navbar-panier.component';

describe('NavbarPanierComponent', () => {
  let component: NavbarPanierComponent;
  let fixture: ComponentFixture<NavbarPanierComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NavbarPanierComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavbarPanierComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
