import { Component } from '@angular/core';
import { GestionUserService } from '../service/gestion-user.service';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { RegisterComponent } from '../register/register.component';
import { LoginComponent } from '../login/login.component';

import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar1',
  templateUrl: './navbar1.component.html',
  styleUrls: ['./navbar1.component.scss']
})
export class Navbar1Component {
 
  title= 'test';
  logged= false;
  auth= "login"
  
  constructor(private ser:GestionUserService , private modalService: NgbModal, private router:Router,){
    this.ser.logchange.subscribe(res=>{
      this.logged=res
    })
    this.auth= this.logged ? "Logout" :"login";
  }

  logout(){
    this.ser.logout()
    this.router.navigate(['/'])
  }


  openLogin() {
    const modalRef = this.modalService.open(LoginComponent);
    modalRef.componentInstance.name = 'World';
  }

  openRegister() {
    this.ser.roleCompte = "client"
    const modalRef = this.modalService.open(RegisterComponent, {size:"lg"});
    modalRef.componentInstance.name = 'World';
  }

}