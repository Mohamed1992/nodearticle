import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CosmetiqueComponent } from './cosmetique.component';

describe('CosmetiqueComponent', () => {
  let component: CosmetiqueComponent;
  let fixture: ComponentFixture<CosmetiqueComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CosmetiqueComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CosmetiqueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
