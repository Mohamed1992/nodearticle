import { Component, OnInit } from '@angular/core';
import { GestionCategorieService } from '../service/gestion-categorie.service';

@Component({
  selector: 'app-cosmetique',
  templateUrl: './cosmetique.component.html',
  styleUrls: ['./cosmetique.component.scss']
})

export class CosmetiqueComponent implements OnInit {

 
  categories
  origineCategorie
  constructor(private serv:GestionCategorieService) {
    
    this.serv.logchange.subscribe( res =>{
      if(res.length > 0){
        var tabCategories = res
          this.origineCategorie = tabCategories.filter( x=> x.nom == "Cosmétique")[0]
          if(this.serv.varCategorieFiltration != null){
              if(this.serv.varCategorieFiltration.categorie != this.origineCategorie.id){
                this.categories = {categorie:this.origineCategorie.id, sousCategorie: "0", sousSousCategorie: "0"}
              }else{
                this.categories = this.serv.varCategorieFiltration
              }
          }else{
            this.categories = {categorie:this.origineCategorie.id, sousCategorie: "0", sousSousCategorie: "0"}
          }
      }
    })

    this.serv.categorieFiltration.subscribe( res =>{
      if(this.origineCategorie){
        if(this.serv.varCategorieFiltration != null){
          if(this.serv.varCategorieFiltration.categorie != this.origineCategorie.id){
            this.categories = {categorie:this.origineCategorie.id, sousCategorie: "0", sousSousCategorie: "0"}
          }else{
            this.categories = this.serv.varCategorieFiltration
          }
        }else{
          this.categories = {categorie:this.origineCategorie.id, sousCategorie: "0", sousSousCategorie: "0"}
        }
      }
    })

   
  }

  

  ngOnInit(): void {

  }

}