import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemPagePanierComponent } from './item-page-panier.component';

describe('ItemPagePanierComponent', () => {
  let component: ItemPagePanierComponent;
  let fixture: ComponentFixture<ItemPagePanierComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemPagePanierComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemPagePanierComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
