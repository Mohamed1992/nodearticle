import { Component, OnInit , Input } from '@angular/core';

@Component({
  selector: 'app-item-page-panier',
  templateUrl: './item-page-panier.component.html',
  styleUrls: ['./item-page-panier.component.scss']
})
export class ItemPagePanierComponent implements OnInit {

  @Input() item

  isPromo = false

  constructor() { }

  ngOnInit(): void {
    if(this.item.article.isPromo == "1"){
      this.isPromo = true
    }else{
      this.isPromo = false
    }
  }

}
