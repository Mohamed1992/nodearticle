import { Component, OnInit } from '@angular/core';
import { GestionArticleService } from '../service/gestion-article.service';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { WebSocketService } from '../service/web-socket.service';

@Component({
  selector: 'app-commande-details',
  templateUrl: './commande-details.component.html',
  styleUrls: ['./commande-details.component.scss']
})
export class CommandeDetailsComponent implements OnInit {
  
  
  constructor(private serv:GestionArticleService, public activeModal: NgbActiveModal, private serv2:WebSocketService) { }
  
  totalPrix=0
  commande
  isAdmin = false
  button1 ="refuse"
  button2 ="accepte"
  isFini = false
  tabItemsCommande = []
  
  ngOnInit(): void {
     let commandes = this.serv.varTabCommandes.filter(x => x.id == this.serv.idCommande)
     this.commande = commandes[0]
     var tabArticles = this.commande.commande
     
     this.tabItemsCommande = []
     
     for(var i = 0; i<tabArticles.length;i++){
         
         var prix = 0

         if(tabArticles[i].isPromo == "1"){
          prix =  Number(tabArticles[i].newPrix)
         }else{
          prix =  Number(tabArticles[i].prix)
         }
  
         let quantite = Number(tabArticles[i].quantite)
         this.totalPrix += prix * quantite
         this.tabItemsCommande.push({prix:prix, image:tabArticles[i].image, nom:tabArticles[i].nom, quantite:tabArticles[i].quantite, ref:tabArticles[i].ref })
      }

      this.isAdmin = localStorage.getItem("Role") == "admin" 
      
      if(this.commande.etat == "accepte"){
        this.button2 = "fini"
      
      }else if(this.commande.etat == "refuse"){
        this.button1 = "enAttent"
      
      }else if(this.commande.etat == "fini"){
        this.isFini = true
      }

      this.changeIsShowAdminCommande()
  }
  
  charge = false

  changeIsShowAdminCommande(){
    if(this.isAdmin && this.commande.isShowAdmin == 0 && this.commande.etat == "enAttent"){
      this.serv.modifierIsShowCommande(this.commande.id).subscribe(res=>{
        if(res.status){
          let nbr = this.serv2.varNumberCommandes
          this.serv2.setNumberCommandes(nbr-1)
          var commandes = this.serv.varTabCommandes
          for(let i = 0; i < commandes.length; i++){
            if(commandes[i].id == this.commande.id){
              commandes[i].isShow =  false
              commandes[i].isShowAdmin =  1
            }
          }
          this.serv.setCommandes(commandes)
        }
      })
    }
  }

  changerCommande(event){
    let etat = event.target.id
    
    if(this.charge){
      return false
    }

    this.charge = true
    this.serv.modifierCommande(etat).subscribe(res=>{
      this.charge = false
      this.commande.etat = etat
      this.serv.varTabCommandes.filter(x => x.id == this.serv.idCommande)[0].etat = etat
      this.serv.setCommandes(this.serv.varTabCommandes)
      this.activeModal.close()
      this.serv.setLoadingCommandes()
    })
  }


}
